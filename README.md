# README #

gem5 code repositories for Static Bubble Deadlock Recovery Scheme

### Design Description ###
Static Bubble: A Framework for Deadlock-free Irregular On-chip Topologies 

Aniruddh Ramrakhyani and Tushar Krishna

In Proc of the 23rd IEEE International Symposium on High-Performance Computer Architecture (HPCA), Feb 2017

	Paper: http://synergy.ece.gatech.edu/wp-content/uploads/sites/332/2016/09/staticbubble_hpca2017.pdf

### How to run ###

* See gem5/README

### Developer ###

* Aniruddh Ramrakhyani (aniruddh.ramrakhyani@gmail.com)