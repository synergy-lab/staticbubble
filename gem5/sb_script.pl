#! /usr/bin/perl

#script for sb scheme

use strict;
use warnings;

my $cmd='echo running sb_script';
system($cmd);

my $i = 0;

#bit-complement synthetic=2 : only for regular mesh

#64 Mesh 

#random routing

$cmd = 'echo 64_mesh_bit_complement_random_routing_without_sb_scheme > mesh_64_bit_compl.txt';
system($cmd);

my $iter = 0;

for($i=0.02; $i<0.30; $i = $i + 0.02)
{
    $cmd = 'echo >> mesh_64_bit_compl.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> mesh_64_bit_compl.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_64_bit_compl.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> mesh_64_bit_compl.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 --topology=Mesh --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$i.' --synthetic=2 --routing-algorithm=2 > temp.txt';

    system($cmd);

    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh64_'.$iter.'_stats.txt';
    system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> mesh_64_bit_compl.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> mesh_64_bit_compl.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_64_bit_compl.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> mesh_64_bit_compl.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_64_bit_compl.txt';
    system($cmd);


    $iter++;
}




#random routing with static bubble

$cmd = 'echo 64_mesh_bit_complement_random_routing_with_sb_scheme > mesh_64_bit_compl_sb.txt';
system($cmd);

$iter = 0;

for($i=0.02; $i<0.30; $i = $i + 0.02)
{
    $cmd = 'echo >> mesh_64_bit_compl_sb.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> mesh_64_bit_compl_sb.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_64_bit_compl_sb.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> mesh_64_bit_compl_sb.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 --topology=Mesh --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$i.' --synthetic=2 --routing-algorithm=2 --enable-static-bubble=1 > temp.txt';

    system($cmd);

    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh64_sb_'.$iter.'_stats.txt';
    system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> mesh_64_bit_compl_sb.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> mesh_64_bit_compl_sb.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_64_bit_compl_sb.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> mesh_64_bit_compl_sb.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_64_bit_compl_sb.txt';
    system($cmd);


    $iter++;
}


#Uniform Random: Synthetic = 0


#random routing

$cmd = 'echo 64_mesh_uniform_random_random_routing_without_sb_scheme > mesh_64_uniform_rand.txt';
system($cmd);

$iter = 0;

for($i=0.02; $i<0.52; $i = $i + 0.02)
{
    $cmd = 'echo >> mesh_64_uniform_rand.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> mesh_64_uniform_rand.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_64_uniform_rand.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> mesh_64_uniform_rand.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 --topology=Mesh --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$i.' --synthetic=0 --routing-algorithm=2 > temp.txt';

    system($cmd);

    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh64_'.$iter.'_ur_stats.txt';
    system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> mesh_64_uniform_rand.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> mesh_64_uniform_rand.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_64_uniform_rand.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> mesh_64_uniform_rand.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_64_bit_uniform_rand.txt';
    system($cmd);


    $iter++;
}


#random routing with static bubble

$cmd = 'echo 64_mesh_uniform_random_random_routing_with_sb_scheme > mesh_64_uniform_rand_sb.txt';
system($cmd);

$iter = 0;

for($i=0.02; $i<0.52; $i = $i + 0.02)
{
    $cmd = 'echo >> mesh_64_uniform_rand_sb.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> mesh_64_uniform_rand_sb.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_64_uniform_rand_sb.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> mesh_64_uniform_rand_sb.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 --topology=Mesh --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$i.' --synthetic=0 --routing-algorithm=2 --enable-static-bubble=1 > temp.txt';

    system($cmd);

    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh64_sb_'.$iter.'_ur_stats.txt';
    system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> mesh_64_uniform_rand_sb.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> mesh_64_uniform_rand_sb.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_64_uniform_rand_sb.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> mesh_64_uniform_rand_sb.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_64_uniform_rand_sb.txt';
    system($cmd);


    $iter++;
}

