#! /usr/bin/perl

use strict;
use warnings;

# run simulation for topologies with 1 to 100 faults
# For each fault number generate 100 different topologies 
# For each configuration sweep the injection rate from 0.02 to 0.4 in steps of 0.02

# Edit: changed
# run simulation for topologies with 1 to 48 faulty routers
# For each fault number generate 100 different topologies 
# For each configuration sweep the injection rate from 0.1 to 0.9 in steps of 0.1


# For LINKS

#my $num_faults = $ARGV[0];

for(my $num_faults=1; $num_faults < 29; $num_faults++)
{

    my $path = '/usr/scratch/aramrakhyani3/routers/fault_'.$num_faults;

    my $cmd= 'mkdir '.$path;
    system($cmd);

    my $sim_path = $path.'/sim';

    $cmd= 'mkdir '.$sim_path;
    system($cmd);

    my $network_stats_path = $sim_path.'/network_stats.txt';
    
    
# generate 100 different topologies for each fault by changing the seed
    for(my $seed=1; $seed<51; $seed++)
    {
	$cmd = 'echo > '.$path.'/t'.$seed.'.txt';
	system($cmd);
	
	# for each topology vary the injection rate from 0.02 to 0.5 in steps of 0.02
	for(my $ir=0.9; $ir<1.0; $ir = $ir + 0.1)
	{
	    $cmd = 'echo ------------------Sim--------------------------- >> '.$path.'/t'.$seed.'.txt';
	    system($cmd);
	    
	    $cmd = 'echo >> '.$path.'/t'.$seed.'.txt';
	    system($cmd);
	
	    $cmd = 'echo injection rate ='.$ir.' >> '.$path.'/t'.$seed.'.txt';
	    system($cmd);
	
	# cmd for bfs
	#$cmd = './build/ALPHA_Network_test/gem5.debug -d '.$sim_path.' configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 --topology=bfs --num-rows=8 --vcs-per-vnet=4 --sim-cycles=10000 --injectionrate='.$ir.' --synthetic=0 --routing-algorithm=0 --enable-sb-fault-model=1 --faulty-links-file='.$faulty_links_file;

	# cmd for sb
	$cmd = './build/ALPHA_Network_test/gem5.debug -d '.$sim_path.' configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 --topology=faulty_routers --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$ir.' --synthetic=0 --routing-algorithm=0 --num-packets-max=63 --enable-sb-fault-model=1 --seed='.$seed.' --num-faults='.$num_faults;


	# cmd for escapevc : 3 vc per vnet
	# $cmd = './build/ALPHA_Network_test/gem5.debug -d '.$sim_path.' configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 --topology=faulty_routers --num-rows=8 --vcs-per-vnet=3 --sim-cycles=10000 --injectionrate='.$ir.' --synthetic=0 --routing-algorithm=0 --enable-sb-fault-model=1 --enable-static-bubble=1 --dd-thresh=126 --seed='.$seed.' --num-faults='.$num_faults;
	
	system($cmd);
	
	$cmd = './my_scripts/extract_network_stats.sh '.$sim_path.' '.$network_stats_path;
	system($cmd);
	$cmd = 'echo >> '.$path.'/t'.$seed.'.txt';
	system($cmd);
	
	$cmd = 'cat '.$network_stats_path.' >> '.$path.'/t'.$seed.'.txt';
	system($cmd);
	
	$cmd = 'echo >> '.$path.'/t'.$seed.'.txt';
	system($cmd);
	
	$cmd = 'echo --------------------end------------------ >> '.$path.'/t'.$seed.'.txt';
	system($cmd);
	
	$cmd = 'echo >> '.$path.'/t'.$seed.'.txt';
	system($cmd);
    }
}
}
