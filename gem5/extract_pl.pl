#! /usr/bin/perl

use strict;
use warnings;

# script to determine the percentage of deadlock-prone topologies 
# look at injection rate=0.9 for each topology for each num_fault case
# if packets_received + total_flits_dropped < packets_injected, it is deadlock prone

my $cmd = 'echo > results.txt';
system($cmd);

for(my $num_faults =30; $num_faults < 31; $num_faults++)
{
    my $path = '/usr/scratch/aniruddh/hpca_results/energy/escapevc/fault_'.$num_faults;

    # there are 100 random topologies for each fault number
    for(my $top_num =1; $top_num < 11; $top_num++)
    {
	my $file = $path.'/t'.$top_num.'.txt';
	
	#$cmd = 'grep "network_deadlock_path_length_sum" '.$file;
	#system($cmd);

	$cmd  = 'grep "packets_received" '.$file.' -m 1 | sed "s/packets_received = \s*//" >> results.txt';

	#$cmd  = 'grep "average_hops" '.$file.' -m 1 | sed "s/average_hops = \s*//" >> results.txt';

	system($cmd);
    }

}
