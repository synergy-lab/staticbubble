echo > network_stats_mine.txt
grep "packets_injected::total" m5out/stats.txt | sed 's/system.ruby.network.packets_injected::total\s*/packets_injected = /' >> network_stats_mine.txt
grep "packets_received::total" m5out/stats.txt | sed 's/system.ruby.network.packets_received::total\s*/packets_received = /' >> network_stats_mine.txt
grep "average_packet_latency" m5out/stats.txt | sed 's/system.ruby.network.average_packet_latency\s*/average_packet_latency = /' >> network_stats_mine.txt
grep "flits_injected::total" m5out/stats.txt | sed 's/system.ruby.network.flits_injected::total\s*/flits_injected = /' >> network_stats_mine.txt
grep "flits_received::total" m5out/stats.txt | sed 's/system.ruby.network.flits_received::total\s*/flits_received = /' >> network_stats_mine.txt
grep "average_flit_latency" m5out/stats.txt | sed 's/system.ruby.network.average_flit_latency\s*/average_flit_latency = /' >> network_stats_mine.txt

grep "total_probes_sent" m5out/stats.txt | sed 's/system.ruby.network.total_probes_sent\s*/total_probes_sent = /' >> network_stats_mine.txt
grep "total_disables_sent" m5out/stats.txt | sed 's/system.ruby.network.total_disables_sent\s*/total_disables_sent = /' >> network_stats_mine.txt
grep "total_enables_sent" m5out/stats.txt | sed 's/system.ruby.network.total_enables_sent\s*/total_enables_sent = /' >> network_stats_mine.txt
grep "total_check_probes_sent" m5out/stats.txt | sed 's/system.ruby.network.total_check_probes_sent\s*/total_check_probes_sent = /' >> network_stats_mine.txt

grep "total_probes_dropped" m5out/stats.txt | sed 's/system.ruby.network.total_probes_dropped\s*/total_probes_dropped = /' >> network_stats_mine.txt
grep "total_disables_dropped" m5out/stats.txt | sed 's/system.ruby.network.total_disables_dropped\s*/total_disables_dropped = /' >> network_stats_mine.txt
grep "total_enables_dropped" m5out/stats.txt | sed 's/system.ruby.network.total_enables_dropped\s*/total_enables_dropped = /' >> network_stats_mine.txt
grep "total_check_probes_dropped" m5out/stats.txt | sed 's/system.ruby.network.total_check_probes_dropped\s*/total_check_probes_dropped = /' >> network_stats_mine.txt

grep "total_sb_switched_on" m5out/stats.txt | sed 's/system.ruby.network.total_sb_switched_on\s*/total_sb_switched_on = /' >> network_stats_mine.txt
grep "network_max_deadlock_path_length" m5out/stats.txt | sed 's/system.ruby.network.network_max_deadlock_path_length\s*/network_max_deadlock_path_length = /' >> network_stats_mine.txt

grep "network_deadlock_path_length_sum" m5out/stats.txt | sed 's/system.ruby.network.network_deadlock_path_length_sum\s*/network_deadlock_path_length_sum = /' >> network_stats_mine.txt

grep "total_sb_vc_copy" m5out/stats.txt | sed 's/system.ruby.network.total_sb_vc_copy\s*/total_sb_vc_copy = /' >> network_stats_mine.txt

grep "probe_link_utilisation" m5out/stats.txt | sed 's/system.ruby.network.probe_link_utilisation\s*/probe_link_utilisation = /' >> network_stats_mine.txt
grep "disable_link_utilisation" m5out/stats.txt | sed 's/system.ruby.network.disable_link_utilisation\s*/disable_link_utilisation = /' >> network_stats_mine.txt
grep "enable_link_utilisation" m5out/stats.txt | sed 's/system.ruby.network.enable_link_utilisation\s*/enable_link_utilisation = /' >> network_stats_mine.txt
grep "check_probe_link_utilisation" m5out/stats.txt | sed 's/system.ruby.network.check_probe_link_utilisation\s*/check_probe_link_utilisation = /' >> network_stats_mine.txt

grep "flit_link_utilisation" m5out/stats.txt | sed 's/system.ruby.network.flit_link_utilisation\s*/flit_link_utilisation = /' >> network_stats_mine.txt
grep "link_utilization" m5out/stats.txt | sed 's/system.ruby.network.link_utilization\s*/link_utilization = /' >> network_stats_mine.txt

grep "total_flits_dropped" m5out/stats.txt | sed 's/system.ruby.network.total_flits_dropped\s*/total_flits_dropped = /' >> network_stats_mine.txt