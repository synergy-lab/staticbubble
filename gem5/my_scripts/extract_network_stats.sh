echo > $2
grep "packets_injected::total" $1/stats.txt | sed 's/system.ruby.network.packets_injected::total\s*/packets_injected = /' >> $2
grep "packets_received::total" $1/stats.txt | sed 's/system.ruby.network.packets_received::total\s*/packets_received = /' >> $2
grep "average_packet_latency" $1/stats.txt | sed 's/system.ruby.network.average_packet_latency\s*/average_packet_latency = /' >> $2
grep "flits_injected::total" $1/stats.txt | sed 's/system.ruby.network.flits_injected::total\s*/flits_injected = /' >> $2
grep "flits_received::total" $1/stats.txt | sed 's/system.ruby.network.flits_received::total\s*/flits_received = /' >> $2
grep "average_flit_latency" $1/stats.txt | sed 's/system.ruby.network.average_flit_latency\s*/average_flit_latency = /' >> $2

grep "total_probes_sent" $1/stats.txt | sed 's/system.ruby.network.total_probes_sent\s*/total_probes_sent = /' >> $2
grep "total_disables_sent" $1/stats.txt | sed 's/system.ruby.network.total_disables_sent\s*/total_disables_sent = /' >> $2
grep "total_enables_sent" $1/stats.txt | sed 's/system.ruby.network.total_enables_sent\s*/total_enables_sent = /' >> $2
grep "total_check_probes_sent" $1/stats.txt | sed 's/system.ruby.network.total_check_probes_sent\s*/total_check_probes_sent = /' >> $2

grep "total_probes_dropped" $1/stats.txt | sed 's/system.ruby.network.total_probes_dropped\s*/total_probes_dropped = /' >> $2
grep "total_disables_dropped" $1/stats.txt | sed 's/system.ruby.network.total_disables_dropped\s*/total_disables_dropped = /' >> $2
grep "total_enables_dropped" $1/stats.txt | sed 's/system.ruby.network.total_enables_dropped\s*/total_enables_dropped = /' >> $2
grep "total_check_probes_dropped" $1/stats.txt | sed 's/system.ruby.network.total_check_probes_dropped\s*/total_check_probes_dropped = /' >> $2

grep "total_sb_switched_on" $1/stats.txt | sed 's/system.ruby.network.total_sb_switched_on\s*/total_sb_switched_on = /' >> $2
grep "network_max_deadlock_path_length" $1/stats.txt | sed 's/system.ruby.network.network_max_deadlock_path_length\s*/network_max_deadlock_path_length = /' >> $2

grep "network_deadlock_path_length_sum" $1/stats.txt | sed 's/system.ruby.network.network_deadlock_path_length_sum\s*/network_deadlock_path_length_sum = /' >> $2

grep "total_sb_vc_copy" $1/stats.txt | sed 's/system.ruby.network.total_sb_vc_copy\s*/total_sb_vc_copy = /' >> $2

grep "probe_link_utilisation" $1/stats.txt | sed 's/system.ruby.network.probe_link_utilisation\s*/probe_link_utilisation = /' >> $2
grep "disable_link_utilisation" $1/stats.txt | sed 's/system.ruby.network.disable_link_utilisation\s*/disable_link_utilisation = /' >> $2
grep "enable_link_utilisation" $1/stats.txt | sed 's/system.ruby.network.enable_link_utilisation\s*/enable_link_utilisation = /' >> $2
grep "check_probe_link_utilisation" $1/stats.txt | sed 's/system.ruby.network.check_probe_link_utilisation\s*/check_probe_link_utilisation = /' >> $2

grep "flit_link_utilisation" $1/stats.txt | sed 's/system.ruby.network.flit_link_utilisation\s*/flit_link_utilisation = /' >> $2
grep "link_utilization" $1/stats.txt | sed 's/system.ruby.network.link_utilization\s*/link_utilization = /' >> $2

grep "total_flits_dropped" $1/stats.txt | sed 's/system.ruby.network.total_flits_dropped\s*/total_flits_dropped = /' >> $2

grep "average_hops" $1/stats.txt | sed 's/system.ruby.network.average_hops\s*/average_hops = /' >> $2