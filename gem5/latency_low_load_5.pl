#! /usr/bin/perl

# For link failures: SB

# script to lauch screens in parallel. For each fault number launch a new screen and call another script

my $screen_name = 'bitcomp_evc';

for(my $fault_num =1; $fault_num <101; $fault_num++)
{
    my $cmd = 'screen -m -d -S '.$screen_name.$fault_num;
    system($cmd);

    $cmd = 'screen -S '.$screen_name.$fault_num.' -p 0 -X stuff "tcsh
"';
    system($cmd);

    $cmd = 'screen -S '.$screen_name.$fault_num.' -p 0 -X stuff "source ./my_scripts/set_env.cshrc
"';
    system($cmd);

    # make a new directory for each fault num

    my $path = '/usr/scratch/aniruddh/hpca_results/camera_ready_results/bit_comp_latency/links/evc/fault_'.$fault_num;

    $cmd = 'mkdir -p '.$path;
    system($cmd);

    $cmd = 'mkdir -p '.$path.'/sim';
    system($cmd);

    $cmd = 'mkdir -p '.$path.'/sim/temp';
    system($cmd);


    my $start_inj_rate = 0.01;
    my $end_inj_rate =0.03;
    my $step = 0.01;

=begin comment
    if($fault_num <= 10)
    {
	$start_inj_rate = 0.1;
	$end_inj_rate = 0.3;
    }
    elsif(($fault_num > 10) && ($fault_num <= 20))
    {
	$start_inj_rate = 0.1;
	$end_inj_rate = 0.3;
    }
    elsif(($fault_num > 20) && ($fault_num <= 30))
    {
	$start_inj_rate = 0.1;
	$end_inj_rate = 0.4;
    }
    elsif(($fault_num > 30) && ($fault_num <= 40))
    {
	$start_inj_rate = 0.3;
	$end_inj_rate = 0.6;
    }
    elsif(($fault_num > 40) && ($fault_num <= 50)) 
    {
	$start_inj_rate = 0.4;
	$end_inj_rate = 0.8;
    }
    elsif(($fault_num > 50) && ($fault_num <= 60)) 
    {
	$start_inj_rate = 0.1;
	$end_inj_rate = 0.3;
    }
    elsif(($fault_num > 60) && ($fault_num <= 70)) 
    {
	$start_inj_rate = 0.1;
	$end_inj_rate = 0.3;
    }
    elsif(($fault_num > 70) && ($fault_num <= 80)) 
    {
	$start_inj_rate = 0.2;
	$end_inj_rate = 0.54;
    }
    elsif(($fault_num > 80) && ($fault_num <= 90)) 
    {
	$start_inj_rate = 0.4;
	$end_inj_rate = 0.84;
    }
    elsif(($fault_num > 90) && ($fault_num <= 100)) 
    {
	$start_inj_rate = 0.7;
	$end_inj_rate = 0.92;
    }
=end comment
=cut


    for(my $ir=$start_inj_rate; $ir<$end_inj_rate; $ir = $ir + $step)
    {
	my $num = $ir * 100;

	$cmd = 'mkdir -p '.$path.'/sim/inj0'.$num;
	system($cmd);
    }
    #$cmd = 'mkdir -p '.$path.'/sim/inj001';
    #system($cmd);

    #$cmd = 'mkdir -p '.$path.'/sim/inj002';
    #system($cmd);

    $cmd = 'screen -S '.$screen_name.$fault_num.' -p 0 -X stuff "perl /nethome/aramrakhyani3/gem5/sb_fault_script_5.pl '.$fault_num.'
"';
    system($cmd);
}
