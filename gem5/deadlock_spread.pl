#! /usr/bin/perl

use strict;
use warnings;

# Script to determine the injection rates at which topologies start to deadlock 

# create a directory in scratch space for results

my $path = '/usr/scratch/aniruddh/deadlock_spread/camera_ready';

my $cmd;
#my $cmd = 'mkdir '.$path;
#system($cmd);

#for each fault num create a new file
for(my $num_fault=1; $num_fault < 101; $num_fault++)
{
    $cmd = 'echo > '.$path.'/res'.$num_fault.'.txt';
    system($cmd);

    # for each fault num find the injection rate at which topologies start to deadlock
    my @count = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

    for(my $topology=1; $topology < 101; $topology++)
    {
	my $file = '/usr/scratch/aniruddh/hpca_results/camera_ready_results/percent_deadlock_heat_map/links/fault_'.$num_fault.'/t'.$topology.'.txt';

	open(my $fh, $file);

	my $found = 0;

	while(my $line = <$fh>)
	{
	    if(index($line, 'injection rate') != -1)
	    {
		chomp($line);
		$line =~ s/^\s+//;
		$line =~ s/\s+$//;

		my $inj_rate = substr($line,18);

		<$fh>;
		<$fh>;

		$line = <$fh>;
		chomp($line);
		
		$line =~ s/^\s+//;
		$line =~ s/\s+$//;
		
		my $packets_injected = substr($line,19);
		

		$line = <$fh>;
		chomp($line);

		$line =~ s/^\s+//;
		$line =~ s/\s+$//;
		
		my $packets_rcvd = substr($line,19);

		for(my $i=0; $i<23; $i++)
		{
		    <$fh>;
		}
		
		$line = <$fh>;
		chomp($line);

		$line =~ s/^\s+//;
		$line =~ s/\s+$//;

		my $packets_dropped = substr($line,22);

		if($packets_rcvd + $packets_dropped > $packets_injected)
		{
		    print("fatal error");
		    exit;
		}

		if($packets_rcvd + $packets_dropped < $packets_injected)
		{
		    $count[$inj_rate - 1]++;
		    $found = 1;
		    last;
		}
	    }
	}

	if($found == 0)
	{
	    $count[9]++;
	}
	
	close($fh);
    }

    for(my $i=0; $i<10; $i++)
    {
	$cmd = 'echo '.$count[$i].' >> '.$path.'/res'.$num_fault.'.txt';
	system($cmd);
    }

}
