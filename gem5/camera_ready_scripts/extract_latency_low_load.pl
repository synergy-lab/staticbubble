#! /usr/bin/perl

use strict;
use warnings;

# script to determine performance loss between minnimal routing and BFS

# Avg. Performance loss: For each fault-number calclulate the average of avg. flit latency for 10 different topologies.

# Max. Performance loss: Out of all topologies for a given fault number, calculate the max flit latency for BFS and min for minimal.

my $path = '/usr/scratch/aniruddh/per_loss';

#my $cmd = 'mkdir '.$path;
#system($cmd);

# There will be two output files corresponding to injection rate of 0.01 and 0.02

my $cmd = 'echo > '.$path.'/inj001.txt';
system($cmd);

$cmd = 'echo > '.$path.'/inj002.txt';
system($cmd);

$cmd = 'echo > '.$path.'/num_tops_1.txt';
system($cmd);

$cmd = 'echo > '.$path.'/num_tops_2.txt';
system($cmd);

for(my $num_faults = 1; $num_faults<=30; $num_faults++)
{
    my $avg_latency_1 = 0;
    my $avg_latency_2 = 0;

    my $num_top_1 = 0;
    my $num_top_2 = 0;

    for(my $i=1; $i<101; $i++)
    {
	my $file = '/usr/scratch/aniruddh/hpca_results/camera_ready_results/bit_comp_latency/routers/evc/fault_'.$num_faults.'/t'.$i.'.txt';
	open(my $fh, $file) or print($file);

	my @temp = read_file($fh);
	close($fh);

	if(!($temp[0]==-1))
	{	    
	    $avg_latency_1 += $temp[0];
	    $num_top_1++;
	}

	if(!($temp[1]==-1))
	{	    
	    $avg_latency_2 += $temp[1];
	    $num_top_2++;
	}
    }

    $avg_latency_1 = $avg_latency_1/$num_top_1;
    $avg_latency_2 = $avg_latency_2/$num_top_2;

    $cmd ='echo '.$avg_latency_1.' >> '.$path.'/inj001.txt';
    system($cmd);

    $cmd ='echo '.$avg_latency_2.' >> '.$path.'/inj002.txt';
    system($cmd);


    $cmd = 'echo '.$num_top_1.' >> '.$path.'/num_tops_1.txt';
    system($cmd);

    $cmd = 'echo '.$num_top_2.' >> '.$path.'/num_tops_2.txt';
    system($cmd);
}

sub read_file 
{
    my $fh = $_[0];

    my @avg_latency;

    while(my $line = <$fh>)
    {
	if(index($line, 'injection rate =0.01') != -1)
	{	
	    for(my $j=1; $j<=4; $j++)
	    {
		<$fh>;
	    }
	    
	    $line = <$fh>;
	    chomp($line);
	    
	    $line =~ s/^\s+//;
	    $line =~ s/\s+$//;
	    
	    if(length($line) >= 25)
	    {
		#if($avg_flit_latency_2 < substr($line,25))
		#{
		#    $avg_latency[1] = substr($line,25);
		#}
		$avg_latency[0] = substr($line,25);
	    }
	    else
	    {
		$avg_latency[0] = -1;
	    }
	    
	}
	
	elsif(index($line, 'injection rate =0.02') != -1)
	{
	    for(my $j=1; $j<=4; $j++)
	    {
		<$fh>;
	    }
	    
	    $line = <$fh>;
	    chomp($line);
	    
	    $line =~ s/^\s+//;
	    $line =~ s/\s+$//;	    
	    
	    if(length($line) >= 25)
	    {
		#if($avg_flit_latency_2 < substr($line,25))
		#{
		#    $avg_latency[1] = substr($line,25);
		#}
		$avg_latency[1] = substr($line,25);
	    }
	    else
	    {
		$avg_latency[1] = -1;
	    }
	    
	    last;
	}
    }

    return @avg_latency;
    
}
