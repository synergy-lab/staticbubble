32_mesh_dark_sillicon_config_1_with_sb_scheme

--------------------sim 0 -----------------

injection rate = 0.02


packets_injected = 6398                      
packets_received = 6382                      
average_packet_latency = 25.732999                      
flits_injected = 6398                      
flits_received = 6382                      
average_flit_latency = 25.732999                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 75450                      
link_utilization = 75450                      

---------------------------------end------------------------------------


--------------------sim 1 -----------------

injection rate = 0.04


packets_injected = 12600                      
packets_received = 12565                      
average_packet_latency = 25.970633                      
flits_injected = 12600                      
flits_received = 12565                      
average_flit_latency = 25.970633                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 149047                      
link_utilization = 149047                      

---------------------------------end------------------------------------


--------------------sim 2 -----------------

injection rate = 0.06


packets_injected = 19072                      
packets_received = 19025                      
average_packet_latency = 26.426281                      
flits_injected = 19072                      
flits_received = 19025                      
average_flit_latency = 26.426281                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 226438                      
link_utilization = 226438                      

---------------------------------end------------------------------------


--------------------sim 3 -----------------

injection rate = 0.08


packets_injected = 25386                      
packets_received = 25332                      
average_packet_latency = 27.016817                      
flits_injected = 25386                      
flits_received = 25332                      
average_flit_latency = 27.016817                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 301524                      
link_utilization = 301524                      

---------------------------------end------------------------------------


--------------------sim 4 -----------------

injection rate = 0.1


packets_injected = 31913                      
packets_received = 31838                      
average_packet_latency = 29.173441                      
flits_injected = 31913                      
flits_received = 31838                      
average_flit_latency = 29.173441                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 379357                      
link_utilization = 379357                      

---------------------------------end------------------------------------


--------------------sim 5 -----------------

injection rate = 0.12


packets_injected = 36642                      
packets_received = 36343                      
average_packet_latency = 191.710948                      
flits_injected = 36642                      
flits_received = 36343                      
average_flit_latency = 191.710948                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 429155                      
link_utilization = 429155                      

---------------------------------end------------------------------------


--------------------sim 6 -----------------

injection rate = 0.14


packets_injected = 36608                      
packets_received = 36229                      
average_packet_latency = 337.283392                      
flits_injected = 36608                      
flits_received = 36229                      
average_flit_latency = 337.283392                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 408683                      
link_utilization = 408683                      

---------------------------------end------------------------------------


--------------------sim 7 -----------------

injection rate = 0.16


packets_injected = 35603                      
packets_received = 35171                      
average_packet_latency = 444.679452                      
flits_injected = 35603                      
flits_received = 35171                      
average_flit_latency = 444.679452                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 389512                      
link_utilization = 389512                      

---------------------------------end------------------------------------


--------------------sim 8 -----------------

injection rate = 0.18


packets_injected = 34633                      
packets_received = 34187                      
average_packet_latency = 640.617662                      
flits_injected = 34633                      
flits_received = 34187                      
average_flit_latency = 640.617662                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 374519                      
link_utilization = 374519                      

---------------------------------end------------------------------------


--------------------sim 9 -----------------

injection rate = 0.2


packets_injected = 33799                      
packets_received = 33334                      
average_packet_latency = 618.724726                      
flits_injected = 33799                      
flits_received = 33334                      
average_flit_latency = 618.724726                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 355308                      
link_utilization = 355308                      

---------------------------------end------------------------------------


--------------------sim 10 -----------------

injection rate = 0.22


packets_injected = 32366                      
packets_received = 31866                      
average_packet_latency = 772.822162                      
flits_injected = 32366                      
flits_received = 31866                      
average_flit_latency = 772.822162                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 328664                      
link_utilization = 328664                      

---------------------------------end------------------------------------


--------------------sim 11 -----------------

injection rate = 0.24


packets_injected = 30807                      
packets_received = 30306                      
average_packet_latency = 716.353560                      
flits_injected = 30807                      
flits_received = 30306                      
average_flit_latency = 716.353560                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 303384                      
link_utilization = 303384                      

---------------------------------end------------------------------------


--------------------sim 12 -----------------

injection rate = 0.26


packets_injected = 30013                      
packets_received = 29478                      
average_packet_latency = 890.761788                      
flits_injected = 30013                      
flits_received = 29478                      
average_flit_latency = 890.761788                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 299566                      
link_utilization = 299566                      

---------------------------------end------------------------------------


--------------------sim 13 -----------------

injection rate = 0.28


packets_injected = 29925                      
packets_received = 29414                      
average_packet_latency = 963.954953                      
flits_injected = 29925                      
flits_received = 29414                      
average_flit_latency = 963.954953                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 298407                      
link_utilization = 298407                      

---------------------------------end------------------------------------


--------------------sim 14 -----------------

injection rate = 0.3


packets_injected = 29417                      
packets_received = 28893                      
average_packet_latency = 1070.338975                      
flits_injected = 29417                      
flits_received = 28893                      
average_flit_latency = 1070.338975                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 287694                      
link_utilization = 287694                      

---------------------------------end------------------------------------


--------------------sim 15 -----------------

injection rate = 0.32


packets_injected = 28269                      
packets_received = 27733                      
average_packet_latency = 1057.904374                      
flits_injected = 28269                      
flits_received = 27733                      
average_flit_latency = 1057.904374                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 273244                      
link_utilization = 273244                      

---------------------------------end------------------------------------


--------------------sim 16 -----------------

injection rate = 0.34


packets_injected = 29300                      
packets_received = 28762                      
average_packet_latency = 1241.363640                      
flits_injected = 29300                      
flits_received = 28762                      
average_flit_latency = 1241.363640                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 285873                      
link_utilization = 285873                      

---------------------------------end------------------------------------


--------------------sim 17 -----------------

injection rate = 0.36


packets_injected = 28904                      
packets_received = 28357                      
average_packet_latency = 1292.667454                      
flits_injected = 28904                      
flits_received = 28357                      
average_flit_latency = 1292.667454                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 282121                      
link_utilization = 282121                      

---------------------------------end------------------------------------


--------------------sim 18 -----------------

injection rate = 0.38


packets_injected = 27865                      
packets_received = 27331                      
average_packet_latency = 1215.830925                      
flits_injected = 27865                      
flits_received = 27331                      
average_flit_latency = 1215.830925                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 269263                      
link_utilization = 269263                      

---------------------------------end------------------------------------


--------------------sim 19 -----------------

injection rate = 0.4


packets_injected = 27719                      
packets_received = 27170                      
average_packet_latency = 1311.328635                      
flits_injected = 27719                      
flits_received = 27170                      
average_flit_latency = 1311.328635                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 267951                      
link_utilization = 267951                      

---------------------------------end------------------------------------


--------------------sim 20 -----------------

injection rate = 0.42


packets_injected = 27060                      
packets_received = 26522                      
average_packet_latency = 1538.421348                      
flits_injected = 27060                      
flits_received = 26522                      
average_flit_latency = 1538.421348                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 261979                      
link_utilization = 261979                      

---------------------------------end------------------------------------


--------------------sim 21 -----------------

injection rate = 0.44


packets_injected = 27468                      
packets_received = 26908                      
average_packet_latency = 1767.573807                      
flits_injected = 27468                      
flits_received = 26908                      
average_flit_latency = 1767.573807                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 263150                      
link_utilization = 263150                      

---------------------------------end------------------------------------


--------------------sim 22 -----------------

injection rate = 0.46


packets_injected = 26963                      
packets_received = 26421                      
average_packet_latency = 1813.822528                      
flits_injected = 26963                      
flits_received = 26421                      
average_flit_latency = 1813.822528                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 260880                      
link_utilization = 260880                      

---------------------------------end------------------------------------


--------------------sim 23 -----------------

injection rate = 0.48


packets_injected = 27351                      
packets_received = 26801                      
average_packet_latency = 2009.596993                      
flits_injected = 27351                      
flits_received = 26801                      
average_flit_latency = 2009.596993                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 264933                      
link_utilization = 264933                      

---------------------------------end------------------------------------


--------------------sim 24 -----------------

injection rate = 0.5


packets_injected = 27503                      
packets_received = 26950                      
average_packet_latency = 2170.612059                      
flits_injected = 27503                      
flits_received = 26950                      
average_flit_latency = 2170.612059                      
total_probes_sent = 0                      
total_disables_sent = 0                      
total_enables_sent = 0                      
total_check_probes_sent = 0                      
total_probes_dropped = 0                      
total_disables_dropped = 0                      
total_enables_dropped = 0                      
total_check_probes_dropped = 0                      
total_sb_switched_on = 0                      
network_max_deadlock_path_length = 0                      
network_deadlock_path_length_sum = 0                      
total_sb_vc_copy = 0                      
probe_link_utilisation = 0                      
system.ruby.network.check_probe_link_utilisation            0                      
disable_link_utilisation = 0                      
enable_link_utilisation = 0                      
check_probe_link_utilisation = 0                      
flit_link_utilisation = 264844                      
link_utilization = 264844                      

---------------------------------end------------------------------------

