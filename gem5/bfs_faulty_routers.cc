
// NOTE: This works only for a 8*8 mesh

#include <iostream>
#include <cstdlib>
#include <time.h>
#include <queue>
#include <vector>
#include <fstream>
#include <cassert>

#define SYSTEM_SIZE 64
#define NUM_ROWS 8
#define NUM_COLS 8

using namespace std;

struct node
{
  int id;
  bool visited;
  node* west;
  node* north;
  node* east;
  node* south;
};

typedef struct node* node_t;

bool contains(vector<int> &vec, int num)
{
  for(int i=0; i<vec.size(); i++)
    {
      if(vec[i] == num)
	return true;
    }

  return false;
}

void shift(vector<int> &unvisited, int element)
{
  //find the element in vector
  int index = -1;
  
  for(int i=0; i<unvisited.size(); i++)
    {
      if(unvisited[i] == element)
	{
	  index = i;
	  break;
	}
    }

  assert(index >= 0);

  for(int i=index; i<unvisited.size() - 1; i++)
    {
      unvisited[i] = unvisited[i+1];
    }

  unvisited.erase(unvisited.begin() + unvisited.size() - 1);
}

void bfs(vector<node_t> &nodes, vector<int> &unvisited, int root)
{
  queue<int> bfsq;

  //mark the node as visited.
  nodes[root]->visited = true;

  //remove it from the unvisited queue
  shift(unvisited, root);

  bfsq.push(root);

  while(bfsq.size() > 0)
    {
      int node_id = bfsq.front();
      bfsq.pop();

      //check neighbours in following order: W->N->E->S. Check first if link is not null.
      //if the neighbour has been already visited, delete the link.
      // else add the neighbours to the queue.

      //west link
      if(nodes[node_id]->west)
	{
	  if(nodes[node_id]->west->visited)
	    {
	      //delete the link
	      nodes[node_id]->west = NULL;
	    }
	  else
	    {
	      //mark west-link node as visited
	      nodes[node_id]->west->visited = true;

	      //remove it from the unvisited queue
	      shift(unvisited, nodes[node_id]->west->id);
	      
	      //push west-link node to queue
	      bfsq.push(nodes[node_id]->west->id);
	    }
	}

      //north link
      if(nodes[node_id]->north)
	{
	  if(nodes[node_id]->north->visited)
	    {
	      //delete the link
	      nodes[node_id]->north = NULL;
	    }
	  else
	    {
	      //mark north-link node as visited
	      nodes[node_id]->north->visited = true;

	      //remove it from the unvisited queue
	      shift(unvisited, nodes[node_id]->north->id);
	      
	      //push north-link node to queue
	      bfsq.push(nodes[node_id]->north->id);
	    }
	}

      //east link
      if(nodes[node_id]->east)
	{
	  if(nodes[node_id]->east->visited)
	    {
	      //delete the link
	      nodes[node_id]->east = NULL;
	    }
	  else
	    {
	      //mark west-link node as visited
	      nodes[node_id]->east->visited = true;

	      //remove it from the unvisited queue
	      shift(unvisited, nodes[node_id]->east->id);
	      
	      //push west-link node to queue
	      bfsq.push(nodes[node_id]->east->id);
	    }
	}

      //south link
      if(nodes[node_id]->south)
	{
	  if(nodes[node_id]->south->visited)
	    {
	      //delete the link
	      nodes[node_id]->south = NULL;
	    }
	  else
	    {
	      //mark west-link node as visited
	      nodes[node_id]->south->visited = true;

	      //remove it from the unvisited queue
	      shift(unvisited, nodes[node_id]->south->id);
	      
	      //push west-link node to queue
	      bfsq.push(nodes[node_id]->south->id);
	    }
	}


    }
}

void print_faulty_links(vector<node_t> &nodes, const char* filename)
{
  ofstream file;
  file.open(filename);

  int link_id = 0;

  //check east-west links first. if link is absent from both nodes, its a faulty link. 

  //east-west links
  for(int row=0; row<NUM_ROWS; row++)
    {
      for(int col=0; col<NUM_COLS-1; col++)
	{
	  int node_id = row*NUM_COLS + col;

	  if((nodes[node_id]->east == NULL) && (nodes[node_id+1]->west == NULL))
	    {
	      file<<link_id<<"\n";
	    }
	  link_id++;
	}
    }

  //north-south links
  for(int row=0; row<NUM_ROWS-1; row++)
    {
      for(int col=0; col<NUM_COLS; col++)
	{
	  int node_id = row*NUM_COLS + col;
	  
	  if((nodes[node_id]->north == NULL) && (nodes[node_id+NUM_COLS]->south == NULL))
	    {
	      file<<link_id<<"\n";
	    }
	  link_id++;
	}
    }
  file.close();
}

int main(int argc, char* argv[])
{
  //create 64-nodes an put it in a vector

  vector<node_t> nodes;

  //for 64 node system
  nodes.reserve(SYSTEM_SIZE);

  for(int i=0; i<SYSTEM_SIZE; i++)
    {
      node_t n = (node_t)malloc(sizeof(node));
      
      //init node
      n->id = i;
      n->visited = false;
      
      n->west = NULL;
      n->north = NULL;
      n->east = NULL;
      n->south = NULL;

      nodes.push_back(n);
    }

  //generate faulty router ids (and get the link-id from it), create horizontal and vertical links without faulty links
  //get num_faults, seed from cmd line

  int max_faults = 49;

  int num_faults = atoi(argv[1]);

  assert(num_faults < max_faults);

  int seed = atoi(argv[2]);

  srand(seed);
  vector<int> rnum;
  vector<int> faulty_routers;

  faulty_routers.reserve(num_faults);

  while(faulty_routers.size() < num_faults)
    {
      int num = rand() % max_faults;

      if(!contains(faulty_routers, num))
	faulty_routers.push_back(num);
    }

  //determine the links corresponding to faulty routers

  for(int i=0; i<num_faults; i++)
    {
      int router_id = faulty_routers[i];
      //cout<<"faulty router id = "<<router_id<<"\n";
      int row_id = router_id / 8;
      int col_id = router_id % 8;

      //east link
      if(col_id < 7)
	{
	  int link_id = row_id*7 + col_id;
	  rnum.push_back(link_id);
	}

      //west-link
      if(col_id > 0)
	{
	  int link_id = row_id*7 + col_id - 1;
	  rnum.push_back(link_id);
	}

      //north-link
      if(row_id <7)
	{
	  int link_id = 56 + row_id*8 + col_id;
	  rnum.push_back(link_id);
	}

      //south-link
      if(row_id > 0)
	{
	  int link_id = 56 + (row_id-1)*8 + col_id;
	  rnum.push_back(link_id);
	}
    }

  int link_id=0;

  //create west-east links: links will be uni-directional so create links from both nodes

  for(int row=0; row<NUM_ROWS; row++)
    {
      for(int col=0; col< NUM_COLS - 1; col++)
	{
	  if(!contains(rnum, link_id))
	    {
	      int node_id = row*NUM_COLS + col;
	      nodes[node_id]->east = nodes[node_id + 1];
	      nodes[node_id + 1]->west = nodes[node_id];
	    }
	  link_id++;
	}
    }

  //create north-south links

  for(int row=0; row<NUM_ROWS -1; row++)
    {
      for(int col=0; col<NUM_COLS; col++)
	{
	  if(!contains(rnum, link_id))
	    {
	      int node_id = row*NUM_COLS + col;
	      nodes[node_id]->north = nodes[node_id + NUM_COLS];
	      nodes[node_id  + NUM_COLS]->south = nodes[node_id];
	    }
	  link_id++;
	}
    }

  //implement BFS: choose a root randomly, create a BFS tree, do this till all the nodes have been covered
  
  vector<int> unvisited;
  
  unvisited.reserve(SYSTEM_SIZE);

  for(int i=0; i<SYSTEM_SIZE; i++)
    {
      unvisited.push_back(i);
    }

  while(unvisited.size() > 0)
    {
      int root = rand() % unvisited.size();
      bfs(nodes, unvisited, unvisited[root]);
    }

  print_faulty_links(nodes, argv[3]);

  return 0;
}
