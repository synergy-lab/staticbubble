#! /usr/bin/perl

#script for running 32 mesh dark sillicon topologies

use strict;
use warnings;

my $cmd='echo running sb_script';
system($cmd);

my $i = 0;
my $iter = 0;

=begin comment
#dark sillicon 32 mesh config 1 without sb

$cmd = 'echo 32_mesh_dark_sillicon_config_1_without_sb_scheme > mesh_32_ds1.txt';
system($cmd);


for($i=0.02; $i<0.52; $i = $i + 0.02)
{
    $cmd = 'echo >> mesh_32_ds1.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> mesh_32_ds1.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds1.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> mesh_32_ds1.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=32 --num-dirs=32 --topology=dark_sillicon_32_mesh_config_1 --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$i.' --synthetic=0 --routing-algorithm=0 > temp1.txt';

    system($cmd);

    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh32_ds1_'.$iter.'_stats.txt';
    system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds1.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> mesh_32_ds1.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds1.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> mesh_32_ds1.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds1.txt ';
    system($cmd);


    $iter++;
}
=end comment
=cut

#dark sillicon 32 mesh config 1 with sb

$cmd = 'echo 32_mesh_dark_sillicon_config_1_with_sb_scheme > /usr/scratch/aniruddh/mesh_32_ds1_sb.txt';
system($cmd);

$iter = 0;

for($i=0.02; $i<0.52; $i = $i + 0.02)
{
    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds1_sb.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> /usr/scratch/aniruddh/mesh_32_ds1_sb.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds1_sb.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> /usr/scratch/aniruddh/mesh_32_ds1_sb.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=32 --num-dirs=32 --topology=dark_sillicon_32_mesh_config_1 --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$i.' --synthetic=0 --routing-algorithm=0 --enable-static-bubble=1 --dd-thresh=66 > /usr/scratch/aniruddh/temp1.txt';

    system($cmd);

#    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh32_ds1_sb_'.$iter.'_stats.txt';
#    system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds1_sb.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> /usr/scratch/aniruddh/mesh_32_ds1_sb.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds1_sb.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> /usr/scratch/aniruddh/mesh_32_ds1_sb.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds1_sb.txt ';
    system($cmd);


    $iter++;
}

=begin comment
#dark sillicon 32 mesh config 2 without sb

$cmd = 'echo 32_mesh_dark_sillicon_config_2_without_sb_scheme > mesh_32_ds2.txt';
system($cmd);

$iter = 0;

for($i=0.02; $i<0.52; $i = $i + 0.02)
{
    $cmd = 'echo >> mesh_32_ds2.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> mesh_32_ds2.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds2.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> mesh_32_ds2.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=32 --num-dirs=32 --topology=dark_sillicon_32_mesh_config_2 --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$i.' --synthetic=0 --routing-algorithm=0 > temp1.txt';

    system($cmd);

    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh32_ds2_'.$iter.'_stats.txt';
    system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds2.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> mesh_32_ds2.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds2.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> mesh_32_ds2.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds2.txt ';
    system($cmd);


    $iter++;
}
=end comment
=cut


#dark sillicon 32 mesh config 2 with sb

$cmd = 'echo 32_mesh_dark_sillicon_config_2_with_sb_scheme > /usr/scratch/aniruddh/mesh_32_ds2_sb.txt';
system($cmd);

$iter = 0;

for($i=0.02; $i<0.52; $i = $i + 0.02)
{
    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds2_sb.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> /usr/scratch/aniruddh/mesh_32_ds2_sb.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds2_sb.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> /usr/scratch/aniruddh/mesh_32_ds2_sb.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=32 --num-dirs=32 --topology=dark_sillicon_32_mesh_config_2 --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$i.' --synthetic=0 --routing-algorithm=0 --enable-static-bubble=1 --dd-thresh=66 > /usr/scratch/aniruddh/temp1.txt';

    system($cmd);

#    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh32_ds2_sb_'.$iter.'_stats.txt';
#    system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds2_sb.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> /usr/scratch/aniruddh/mesh_32_ds2_sb.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds2_sb.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> /usr/scratch/aniruddh/mesh_32_ds2_sb.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds2_sb.txt ';
    system($cmd);


    $iter++;
}

=begin comment
#dark sillicon 32 mesh config 3 without sb

$cmd = 'echo 32_mesh_dark_sillicon_config_3_without_sb_scheme > mesh_32_ds3.txt';
system($cmd);

$iter = 0;

for($i=0.02; $i<0.52; $i = $i + 0.02)
{
    $cmd = 'echo >> mesh_32_ds3.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> mesh_32_ds3.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds3.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> mesh_32_ds3.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=32 --num-dirs=32 --topology=dark_sillicon_32_mesh_config_3 --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$i.' --synthetic=0 --routing-algorithm=0 > temp1.txt';

    system($cmd);

    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh32_ds3_'.$iter.'_stats.txt';
    system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds3.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> mesh_32_ds3.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds3.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> mesh_32_ds3.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds3.txt ';
    system($cmd);


    $iter++;
}
=end comment
=cut

#dark sillicon 32 mesh config 3 with sb

$cmd = 'echo 32_mesh_dark_sillicon_config_3_with_sb_scheme > /usr/scratch/aniruddh/mesh_32_ds3_sb.txt';
system($cmd);

$iter = 0;

for($i=0.02; $i<0.52; $i = $i + 0.02)
{
    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds3_sb.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> /usr/scratch/aniruddh/mesh_32_ds3_sb.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds3_sb.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> /usr/scratch/aniruddh/mesh_32_ds3_sb.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=32 --num-dirs=32 --topology=dark_sillicon_32_mesh_config_3 --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$i.' --synthetic=0 --routing-algorithm=0 --enable-static-bubble=1 --dd-thresh=66 > /usr/scratch/aniruddh/temp1.txt';

    system($cmd);

#    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh32_ds3_sb_'.$iter.'_stats.txt';
#    system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds3_sb.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> /usr/scratch/aniruddh/mesh_32_ds3_sb.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds3_sb.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> /usr/scratch/aniruddh/mesh_32_ds3_sb.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds3_sb.txt ';
    system($cmd);


    $iter++;
}

=begin comment
#dark sillicon 32 mesh config 4 without sb

$cmd = 'echo 32_mesh_dark_sillicon_config_4_without_sb_scheme > mesh_32_ds4.txt';
system($cmd);

$iter = 0;

for($i=0.02; $i<0.52; $i = $i + 0.02)
{
    $cmd = 'echo >> mesh_32_ds4.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> mesh_32_ds4.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds4.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> mesh_32_ds4.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=32 --num-dirs=32 --topology=dark_sillicon_32_mesh_config_4 --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$i.' --synthetic=0 --routing-algorithm=0 > temp1.txt';

    system($cmd);

    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh32_ds4_'.$iter.'_stats.txt';
    system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds4.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> mesh_32_ds4.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds4.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> mesh_32_ds4.txt ';
    system($cmd);

    $cmd = 'echo >> mesh_32_ds4.txt ';
    system($cmd);


    $iter++;
}
=end comment
=cut

#dark sillicon 32 mesh config 4 with sb

$cmd = 'echo 32_mesh_dark_sillicon_config_4_with_sb_scheme > /usr/scratch/aniruddh/mesh_32_ds4_sb.txt';
system($cmd);

$iter = 0;

for($i=0.02; $i<0.52; $i = $i + 0.02)
{
    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds4_sb.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> /usr/scratch/aniruddh/mesh_32_ds4_sb.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds4_sb.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> /usr/scratch/aniruddh/mesh_32_ds4_sb.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=32 --num-dirs=32 --topology=dark_sillicon_32_mesh_config_4 --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$i.' --synthetic=0 --routing-algorithm=0 --enable-static-bubble=1 --dd-thresh=66 > /usr/scratch/aniruddh/temp1.txt';

    system($cmd);

#    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh32_ds4_sb_'.$iter.'_stats.txt';
#    system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds4_sb.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> /usr/scratch/aniruddh/mesh_32_ds4_sb.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds4_sb.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> /usr/scratch/aniruddh/mesh_32_ds4_sb.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/mesh_32_ds4_sb.txt ';
    system($cmd);


    $iter++;
}
