#! /usr/bin/perl

#script for generating correctness graph : 32 mesh config 3 & 16 mesh config 4 with routing table based routing

use strict;
use warnings;

my $cmd='echo running sb_script';
system($cmd);

my $i = 0;

$cmd = 'echo > /usr/scratch/aniruddh/correctness_results_32.txt';
system($cmd);

my $iter = 0;

#dark sillicon 32 mesh config 3 without sb


for($i=0.02; $i<0.72; $i = $i + 0.02)
{
    $cmd = 'echo >> /usr/scratch/aniruddh/correctness_results_32.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> /usr/scratch/aniruddh/correctness_results_32.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/correctness_results_32.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> /usr/scratch/aniruddh/correctness_results_32.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=32 --num-dirs=32 --topology=dark_sillicon_32_mesh_config_3 --num-rows=8 --vcs-per-vnet=8 --sim-cycles=100000 --injectionrate='.$i.' --synthetic=0 --num-packets-max=150 --routing-algorithm=0 > /usr/scratch/aniruddh/temp1.txt';

    system($cmd);

#    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh32_ds3_'.$iter.'_stats.txt';
#   system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/correctness_results_32.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> /usr/scratch/aniruddh/correctness_results_32.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/correctness_results_32.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> /usr/scratch/aniruddh/correctness_results_32.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/correctness_results_32.txt ';
    system($cmd);


    $iter++;
}

#dark sillicon 16 mesh config 4 without sb

$cmd = 'echo > /usr/scratch/aniruddh/correctness_results_16.txt';
system($cmd);

$iter = 0;

for($i=0.02; $i<0.72; $i = $i + 0.02)
{
    $cmd = 'echo >> /usr/scratch/aniruddh/correctness_results_16.txt ';
    system($cmd);

    $cmd = 'echo --------------------sim '.$iter.' ----------------- >> /usr/scratch/aniruddh/correctness_results_16.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/correctness_results_16.txt ';
    system($cmd);

    $cmd = 'echo injection rate = '.$i.' >> /usr/scratch/aniruddh/correctness_results_16.txt ';
    system($cmd);  
    
    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=16 --num-dirs=16 --topology=dark_sillicon_16_mesh_config_4 --num-rows=8 --vcs-per-vnet=8 --sim-cycles=100000 --injectionrate='.$i.' --synthetic=0 --num-packets-max=250 --routing-algorithm=0 > /usr/scratch/aniruddh/temp1.txt';

    system($cmd);

#    $cmd = 'cp ./m5out/stats.txt ./m5out/sb_results/mesh16_ds4_'.$iter.'_stats.txt';
#    system($cmd);

    $cmd = './my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/correctness_results_16.txt ';
    system($cmd);

    $cmd = 'cat network_stats.txt >> /usr/scratch/aniruddh/correctness_results_16.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/correctness_results_16.txt ';
    system($cmd);

    $cmd = 'echo ---------------------------------end------------------------------------ >> /usr/scratch/aniruddh/correctness_results_16.txt ';
    system($cmd);

    $cmd = 'echo >> /usr/scratch/aniruddh/correctness_results_16.txt ';
    system($cmd);


    $iter++;
}

