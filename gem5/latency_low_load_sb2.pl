
#! /usr/bin/perl

# For link failures: SB

# script to lauch screens in parallel. For each fault number launch a new screen and call another script

#my @faults = (3, 10, 15, 20, 30, 40); #for links

#my @faults = (2, 5, 8, 12, 15, 20); #for routers

my @faults = (2, 7, 15, 20, 30); #for energy

my $scr_name = 'ensb';

for(my $iter =0; $iter < 5; $iter++)
{
    my $fault_num = $faults[$iter];

    my $cmd = 'screen -m -d -S '.$scr_name.$fault_num;
    system($cmd);

    $cmd = 'screen -S '.$scr_name.$fault_num.' -p 0 -X stuff "tcsh
"';
    system($cmd); 

    $cmd = 'screen -S '.$scr_name.$fault_num.' -p 0 -X stuff "source ./my_scripts/set_env.cshrc
"';
    system($cmd);

    # make a new directory for each fault num

    my $path = '/usr/scratch/aniruddh/hpca_results/energy/sb/fault_'.$fault_num;

    $cmd = 'mkdir -p '.$path;
    system($cmd);

    $cmd = 'mkdir -p '.$path.'/sim';
    system($cmd);

    $cmd = 'mkdir -p '.$path.'/sim/temp';
    system($cmd);


    my $start_inj_rate;
    my $end_inj_rate;
    my $step;

 if($fault_num == 2)
    {
	$start_inj_rate = 0.06;
	$end_inj_rate = 0.07;
	$step = 0.01;
    }
    elsif($fault_num == 7)
    {
	$start_inj_rate = 0.07;
	$end_inj_rate = 0.08;
	$step = 0.01;
    }
    elsif($fault_num == 15)
    {
	$start_inj_rate = 0.12;
	$end_inj_rate = 0.13;
	$step = 0.01;
    }
    elsif($fault_num == 20)
    {
	$start_inj_rate = 0.22;
	$end_inj_rate = 0.23;
	$step = 0.01;
    }
    elsif($fault_num == 30)
    {
	$start_inj_rate = 0.24;
	$end_inj_rate = 0.25;
	$step = 0.01;
    }


    for(my $ir=$start_inj_rate; $ir<$end_inj_rate; $ir = $ir + $step)
    {
	my $num = $ir * 1000;

	if($num>=100)
	{
	    $cmd = 'mkdir -p '.$path.'/sim/inj0'.$num;
	}
	elsif($num>=10)
	{
	    $cmd = 'mkdir -p '.$path.'/sim/inj00'.$num;
	}
	else
	{
	    $cmd = 'mkdir -p '.$path.'/sim/inj000'.$num;
	}
	
	system($cmd);
    }
    #$cmd = 'mkdir -p '.$path.'/sim/inj001';
    #system($cmd);

    #$cmd = 'mkdir -p '.$path.'/sim/inj002';
    #system($cmd);

    $cmd = 'screen -S '.$scr_name.$fault_num.' -p 0 -X stuff "perl /nethome/aramrakhyani3/gem5/sb_fault_script_sb2.pl '.$fault_num.'
"';
    system($cmd);
}
