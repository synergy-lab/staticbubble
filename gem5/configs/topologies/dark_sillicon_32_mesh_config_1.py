# Copyright, Georgia Institute of Technology
#
# Authors: Aniruddh Ramrakhyani
#
# 8*8 Mesh with 32 cores on. Config 1: Deadlock free

from m5.params import *
from m5.objects import *

from BaseTopology import SimpleTopology

class dark_sillicon_32_mesh_config_1(SimpleTopology):
    description='8*8 Mesh with 32 cores on. Config 1: Deadlock free'

    def __init__(self, controllers):
        self.nodes = controllers

    def makeTopology(self, options, network, IntLink, ExtLink, Router):

        assert(options.num_cpus == 32)
        assert(options.num_rows == 8)

        print "Inside dark sillicon 32 mesh config 1"
        
        nodes = self.nodes

        num_routers = 64
        num_rows = 8
        num_columns = 8

        #only 32 cores are on
        num_cpus = 32

        #calculate no of controllers per router
        cntrls_per_router, remainder = divmod(len(nodes), num_cpus)

        # Create the routers in the mesh
        routers = [Router(router_id=i) for i in range(num_routers)]
        network.routers = routers

        # link counter to set unique link ids
        link_count = 0

        #define the core-ids that will be on in the topology
        on_routers_id = []

        # 32 router id assignment statements

        #row 1
        on_routers_id.append(0)
        on_routers_id.append(1)
        on_routers_id.append(2)
        on_routers_id.append(3)
        on_routers_id.append(5)

        #row 2
        on_routers_id.append(8)
        on_routers_id.append(11)
        on_routers_id.append(12)
        on_routers_id.append(13)
        on_routers_id.append(14)

        #row 3
        on_routers_id.append(16)
        on_routers_id.append(17)
        on_routers_id.append(18)
        on_routers_id.append(22)

        #row 4
        on_routers_id.append(24)
        on_routers_id.append(26)
        on_routers_id.append(28)
        on_routers_id.append(29)
        on_routers_id.append(30)

        #row 5
        on_routers_id.append(33)
        on_routers_id.append(34)
        on_routers_id.append(36)

        #row 6
        on_routers_id.append(41)
        on_routers_id.append(44)
        on_routers_id.append(47)

        #row 7
        on_routers_id.append(49)
        on_routers_id.append(52)
        on_routers_id.append(53)
        on_routers_id.append(55)

        #row 8
        on_routers_id.append(61)
        on_routers_id.append(62)
        on_routers_id.append(63)


        #Distribute Network Nodes uniformly across the on-cores
        network_nodes = []
        remainder_nodes = []

        for node_index in xrange(len(nodes)):
            if node_index < (len(nodes) - remainder):
                network_nodes.append(nodes[node_index])
            else:
                remainder_nodes.append(nodes[node_index])

        # Connect each node to the appropriate router
        ext_links = []
        for (i, n) in enumerate(network_nodes):
            index = i % num_cpus
            router_id = on_routers_id[index];
            ext_links.append(ExtLink(link_id=link_count, ext_node=n,int_node=routers[router_id]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        # Connect the remainding nodes to first router that is on.  These should only be
        # DMA nodes.
        for (i, node) in enumerate(remainder_nodes):
            assert(node.type == 'DMA_Controller')
            assert(i < remainder)
            index = on_routers_id(0)
            ext_links.append(ExtLink(link_id=link_count, ext_node=node, int_node=routers[index]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        network.ext_links = ext_links

        #Constants defining port_directions
        west_id = 1
        south_id = 2
        east_id = 3
        north_id = 4

        #Create Internal links: connect the on routers
        int_links = []

        #Router 0
        int_links.append(IntLink(link_id=link_count, node_a=routers[0], node_b=routers[1], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[0], node_b=routers[8], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1        

        #Router 1
        int_links.append(IntLink(link_id=link_count, node_a=routers[1], node_b=routers[2], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 2
        int_links.append(IntLink(link_id=link_count, node_a=routers[2], node_b=routers[3], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 3
        int_links.append(IntLink(link_id=link_count, node_a=routers[3], node_b=routers[11], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 5
        int_links.append(IntLink(link_id=link_count, node_a=routers[5], node_b=routers[13], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 8
        int_links.append(IntLink(link_id=link_count, node_a=routers[8], node_b=routers[16], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 11
        int_links.append(IntLink(link_id=link_count, node_a=routers[11], node_b=routers[12], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 12
        int_links.append(IntLink(link_id=link_count, node_a=routers[12], node_b=routers[13], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 13
        int_links.append(IntLink(link_id=link_count, node_a=routers[13], node_b=routers[14], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 14
        int_links.append(IntLink(link_id=link_count, node_a=routers[14], node_b=routers[22], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 16
        int_links.append(IntLink(link_id=link_count, node_a=routers[16], node_b=routers[17], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[16], node_b=routers[24], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 17
        int_links.append(IntLink(link_id=link_count, node_a=routers[17], node_b=routers[18], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 18
        int_links.append(IntLink(link_id=link_count, node_a=routers[18], node_b=routers[26], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 22
        int_links.append(IntLink(link_id=link_count, node_a=routers[22], node_b=routers[30], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 26
        int_links.append(IntLink(link_id=link_count, node_a=routers[26], node_b=routers[34], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 28
        int_links.append(IntLink(link_id=link_count, node_a=routers[28], node_b=routers[29], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[28], node_b=routers[36], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 29
        int_links.append(IntLink(link_id=link_count, node_a=routers[29], node_b=routers[30], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 33
        int_links.append(IntLink(link_id=link_count, node_a=routers[33], node_b=routers[34], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[33], node_b=routers[41], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 36
        int_links.append(IntLink(link_id=link_count, node_a=routers[36], node_b=routers[44], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 41
        int_links.append(IntLink(link_id=link_count, node_a=routers[41], node_b=routers[49], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 44
        int_links.append(IntLink(link_id=link_count, node_a=routers[44], node_b=routers[52], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 47
        int_links.append(IntLink(link_id=link_count, node_a=routers[47], node_b=routers[55], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 52
        int_links.append(IntLink(link_id=link_count, node_a=routers[52], node_b=routers[53], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 53
        int_links.append(IntLink(link_id=link_count, node_a=routers[53], node_b=routers[61], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 55
        int_links.append(IntLink(link_id=link_count, node_a=routers[55], node_b=routers[63], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 61
        int_links.append(IntLink(link_id=link_count, node_a=routers[61], node_b=routers[62], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 62
        int_links.append(IntLink(link_id=link_count, node_a=routers[62], node_b=routers[63], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        network.int_links = int_links
