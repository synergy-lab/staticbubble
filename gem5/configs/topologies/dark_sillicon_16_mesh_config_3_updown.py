# Copyright, Georgia Institute of Technology
#
# Authors: Aniruddh Ramrakhyani
#
###################### UP-DOWN ####################
#
# 8*8 Mesh with 16 cores on. Config 3: Not Deadlock free

from m5.params import *
from m5.objects import *

from BaseTopology import SimpleTopology

class dark_sillicon_16_mesh_config_3_updown(SimpleTopology):
    description='8*8 Mesh with 16 cores on. Config 3: Not Deadlock free  ###UP-DOWN###'


    def __init__(self, controllers):
        self.nodes = controllers

    def makeTopology(self, options, network, IntLink, ExtLink, Router):

        assert(options.num_cpus == 16)
        assert(options.num_rows == 8)

        print "Inside dark sillicon 16 mesh config 3   ###UP-DOWN### "
        
        nodes = self.nodes

        num_routers = 64
        num_rows = 8
        num_columns = 8

        #only 16 cores are on
        num_cpus = 16

        #calculate no of controllers per router
        cntrls_per_router, remainder = divmod(len(nodes), num_cpus)

        # Create the routers in the mesh
        routers = [Router(router_id=i) for i in range(num_routers)]
        network.routers = routers

        # link counter to set unique link ids
        link_count = 0

        #define the core-ids that will be on in the topology
        on_routers_id = []

        # 16 router id assignment statements

        #row 3
        on_routers_id.append(17)
        on_routers_id.append(18)

        #row 4
        on_routers_id.append(25)
        on_routers_id.append(26)
        on_routers_id.append(27)
        on_routers_id.append(28)

        #row 5
        on_routers_id.append(32)
        on_routers_id.append(33)
        on_routers_id.append(35)
        on_routers_id.append(36)

        #row 6
        on_routers_id.append(40)
        on_routers_id.append(41)
        on_routers_id.append(42)
        on_routers_id.append(43)

        #row 7
        on_routers_id.append(50)
        on_routers_id.append(51)

        #Distribute Network Nodes uniformly across the on-cores
        network_nodes = []
        remainder_nodes = []

        for node_index in xrange(len(nodes)):
            if node_index < (len(nodes) - remainder):
                network_nodes.append(nodes[node_index])
            else:
                remainder_nodes.append(nodes[node_index])

        # Connect each node to the appropriate router
        ext_links = []
        for (i, n) in enumerate(network_nodes):
            index = i % num_cpus
            router_id = on_routers_id[index];
            ext_links.append(ExtLink(link_id=link_count, ext_node=n,int_node=routers[router_id]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        # Connect the remainding nodes to first router that is on.  These should only be
        # DMA nodes.
        for (i, node) in enumerate(remainder_nodes):
            assert(node.type == 'DMA_Controller')
            assert(i < remainder)
            index = on_routers_id(0)
            ext_links.append(ExtLink(link_id=link_count, ext_node=node, int_node=routers[index]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        network.ext_links = ext_links

        #Constants defining port_directions
        west_id = 1
        south_id = 2
        east_id = 3
        north_id = 4

        #Create Internal links: connect the on routers
        int_links = []


        #Router 17
        int_links.append(IntLink(link_id=link_count, node_a=routers[17], node_b=routers[25], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[17], node_b=routers[18], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 18
        int_links.append(IntLink(link_id=link_count, node_a=routers[18], node_b=routers[26], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 25
        int_links.append(IntLink(link_id=link_count, node_a=routers[25], node_b=routers[33], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

#        int_links.append(IntLink(link_id=link_count, node_a=routers[25], node_b=routers[26], node_a_port=east_id, node_b_port=west_id, weight=1))
#        link_count += 1

        #Router 26
        int_links.append(IntLink(link_id=link_count, node_a=routers[26], node_b=routers[27], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 27
#        int_links.append(IntLink(link_id=link_count, node_a=routers[27], node_b=routers[35], node_a_port=north_id, node_b_port=south_id, weight=1))
#        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[27], node_b=routers[28], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 28
#        int_links.append(IntLink(link_id=link_count, node_a=routers[28], node_b=routers[36], node_a_port=north_id, node_b_port=south_id, weight=1))
#        link_count += 1

        #Router 32
        int_links.append(IntLink(link_id=link_count, node_a=routers[32], node_b=routers[33], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[32], node_b=routers[40], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 33
#        int_links.append(IntLink(link_id=link_count, node_a=routers[33], node_b=routers[41], node_a_port=north_id, node_b_port=south_id, weight=1))
#        link_count += 1

        #Router 35
        int_links.append(IntLink(link_id=link_count, node_a=routers[35], node_b=routers[43], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[35], node_b=routers[36], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 40
        int_links.append(IntLink(link_id=link_count, node_a=routers[40], node_b=routers[41], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 41
        int_links.append(IntLink(link_id=link_count, node_a=routers[41], node_b=routers[42], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 42
        int_links.append(IntLink(link_id=link_count, node_a=routers[42], node_b=routers[50], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

#        int_links.append(IntLink(link_id=link_count, node_a=routers[42], node_b=routers[43], node_a_port=east_id, node_b_port=west_id, weight=1))
#        link_count += 1

        #Router 43
        int_links.append(IntLink(link_id=link_count, node_a=routers[43], node_b=routers[51], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 50
        int_links.append(IntLink(link_id=link_count, node_a=routers[50], node_b=routers[51], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        network.int_links = int_links
