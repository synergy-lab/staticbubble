# Copyright, Georgia Institute of Technology
#
# Authors: Aniruddh Ramrakhyani
#
# 8*8 Mesh with 16 cores on. Config 1: Deadlock free

from m5.params import *
from m5.objects import *

from BaseTopology import SimpleTopology

class dark_sillicon_16_mesh_config_1(SimpleTopology):
    description='8*8 Mesh with 16 cores on. Config 1: Deadlock free'

    def __init__(self, controllers):
        self.nodes = controllers

    def makeTopology(self, options, network, IntLink, ExtLink, Router):

        assert(options.num_cpus == 16)
        assert(options.num_rows == 8)

        print "Inside dark sillicon 16 mesh config 1"
        
        nodes = self.nodes

        num_routers = 64
        num_rows = 8
        num_columns = 8

        #only 16 cores are on
        num_cpus = 16

        #calculate no of controllers per router
        cntrls_per_router, remainder = divmod(len(nodes), num_cpus)

        # Create the routers in the mesh
        routers = [Router(router_id=i) for i in range(num_routers)]
        network.routers = routers

        # link counter to set unique link ids
        link_count = 0

        #define the core-ids that will be on in the topology
        on_routers_id = []

        # 16 router id assignment statements

        #row 3
        on_routers_id.append(17)
        on_routers_id.append(22)

        #row 4
        on_routers_id.append(25)
        on_routers_id.append(26)
        on_routers_id.append(29)
        on_routers_id.append(30)

        #row 5
        on_routers_id.append(34)
        on_routers_id.append(35)
        on_routers_id.append(36)
        on_routers_id.append(37)

        #row 6
        on_routers_id.append(41)
        on_routers_id.append(42)
        on_routers_id.append(45)
        on_routers_id.append(46)

        #row 7
        on_routers_id.append(49)
        on_routers_id.append(54)

        #Distribute Network Nodes uniformly across the on-cores
        network_nodes = []
        remainder_nodes = []

        for node_index in xrange(len(nodes)):
            if node_index < (len(nodes) - remainder):
                network_nodes.append(nodes[node_index])
            else:
                remainder_nodes.append(nodes[node_index])

        # Connect each node to the appropriate router
        ext_links = []
        for (i, n) in enumerate(network_nodes):
            index = i % num_cpus
            router_id = on_routers_id[index];
            ext_links.append(ExtLink(link_id=link_count, ext_node=n,int_node=routers[router_id]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        # Connect the remainding nodes to first router that is on.  These should only be
        # DMA nodes.
        for (i, node) in enumerate(remainder_nodes):
            assert(node.type == 'DMA_Controller')
            assert(i < remainder)
            index = on_routers_id(0)
            ext_links.append(ExtLink(link_id=link_count, ext_node=node, int_node=routers[index]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        network.ext_links = ext_links

        #Constants defining port_directions
        west_id = 1
        south_id = 2
        east_id = 3
        north_id = 4

        #Create Internal links: connect the on routers
        int_links = []


        #Router 17
        int_links.append(IntLink(link_id=link_count, node_a=routers[17], node_b=routers[25], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 22
        int_links.append(IntLink(link_id=link_count, node_a=routers[22], node_b=routers[30], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 25
        int_links.append(IntLink(link_id=link_count, node_a=routers[25], node_b=routers[26], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 26
        int_links.append(IntLink(link_id=link_count, node_a=routers[26], node_b=routers[34], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 29
        int_links.append(IntLink(link_id=link_count, node_a=routers[29], node_b=routers[37], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[29], node_b=routers[30], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 34
        int_links.append(IntLink(link_id=link_count, node_a=routers[34], node_b=routers[42], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[34], node_b=routers[35], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 35
        int_links.append(IntLink(link_id=link_count, node_a=routers[35], node_b=routers[36], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 36
        int_links.append(IntLink(link_id=link_count, node_a=routers[36], node_b=routers[37], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 37
        int_links.append(IntLink(link_id=link_count, node_a=routers[37], node_b=routers[45], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 41
        int_links.append(IntLink(link_id=link_count, node_a=routers[41], node_b=routers[49], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[41], node_b=routers[42], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 45
        int_links.append(IntLink(link_id=link_count, node_a=routers[45], node_b=routers[46], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 46
        int_links.append(IntLink(link_id=link_count, node_a=routers[46], node_b=routers[54], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        network.int_links = int_links
