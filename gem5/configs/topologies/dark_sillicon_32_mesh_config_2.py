# Copyright, Georgia Institute of Technology
#
# Authors: Aniruddh Ramrakhyani
#
# 8*8 Mesh with 32 cores on. Config 2: Deadlock free

from m5.params import *
from m5.objects import *

from BaseTopology import SimpleTopology

class dark_sillicon_32_mesh_config_2(SimpleTopology):
    description='8*8 Mesh with 32 cores on. Config 2: Deadlock free'

    def __init__(self, controllers):
        self.nodes = controllers

    def makeTopology(self, options, network, IntLink, ExtLink, Router):

        assert(options.num_cpus == 32)
        assert(options.num_rows == 8)

        print "Inside dark sillicon 32 mesh config 2"
        
        nodes = self.nodes

        num_routers = 64
        num_rows = 8
        num_columns = 8

        #only 32 cores are on
        num_cpus = 32

        #calculate no of controllers per router
        cntrls_per_router, remainder = divmod(len(nodes), num_cpus)

        # Create the routers in the mesh
        routers = [Router(router_id=i) for i in range(num_routers)]
        network.routers = routers

        # link counter to set unique link ids
        link_count = 0

        #define the core-ids that will be on in the topology
        on_routers_id = []

        # 32 router id assignment statements

        #row 1
        on_routers_id.append(1)
        on_routers_id.append(2)
        on_routers_id.append(3)
        on_routers_id.append(4)
        on_routers_id.append(5)
        on_routers_id.append(6)
        on_routers_id.append(7)

        #row 2
        on_routers_id.append(11)
        on_routers_id.append(13)

        #row 3
        on_routers_id.append(16)
        on_routers_id.append(17)
        on_routers_id.append(18)
        on_routers_id.append(19)
        on_routers_id.append(21)

        #row 4
        on_routers_id.append(24)
        on_routers_id.append(27)
        on_routers_id.append(29)
        on_routers_id.append(30)

        #row 5
        on_routers_id.append(32)
        on_routers_id.append(38)

        #row 6
        on_routers_id.append(40)
        on_routers_id.append(41)
        on_routers_id.append(42)
        on_routers_id.append(46)
        on_routers_id.append(47)

        #row 7
        on_routers_id.append(50)

        #row 8
        on_routers_id.append(58)
        on_routers_id.append(59)
        on_routers_id.append(60)
        on_routers_id.append(61)
        on_routers_id.append(62)
        on_routers_id.append(63)

        #Distribute Network Nodes uniformly across the on-cores
        network_nodes = []
        remainder_nodes = []

        for node_index in xrange(len(nodes)):
            if node_index < (len(nodes) - remainder):
                network_nodes.append(nodes[node_index])
            else:
                remainder_nodes.append(nodes[node_index])

        # Connect each node to the appropriate router
        ext_links = []
        for (i, n) in enumerate(network_nodes):
            index = i % num_cpus
            router_id = on_routers_id[index];
            ext_links.append(ExtLink(link_id=link_count, ext_node=n,int_node=routers[router_id]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        # Connect the remainding nodes to first router that is on.  These should only be
        # DMA nodes.
        for (i, node) in enumerate(remainder_nodes):
            assert(node.type == 'DMA_Controller')
            assert(i < remainder)
            index = on_routers_id(0)
            ext_links.append(ExtLink(link_id=link_count, ext_node=node, int_node=routers[index]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        network.ext_links = ext_links

        #Constants defining port_directions
        west_id = 1
        south_id = 2
        east_id = 3
        north_id = 4

        #Create Internal links: connect the on routers
        int_links = []

        #Router 1
        int_links.append(IntLink(link_id=link_count, node_a=routers[1], node_b=routers[2], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 2
        int_links.append(IntLink(link_id=link_count, node_a=routers[2], node_b=routers[3], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 3
        int_links.append(IntLink(link_id=link_count, node_a=routers[3], node_b=routers[4], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[3], node_b=routers[11], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 4
        int_links.append(IntLink(link_id=link_count, node_a=routers[4], node_b=routers[5], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 5
        int_links.append(IntLink(link_id=link_count, node_a=routers[5], node_b=routers[6], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[5], node_b=routers[13], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 6
        int_links.append(IntLink(link_id=link_count, node_a=routers[6], node_b=routers[7], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 11
        int_links.append(IntLink(link_id=link_count, node_a=routers[11], node_b=routers[19], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 13
        int_links.append(IntLink(link_id=link_count, node_a=routers[13], node_b=routers[21], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 16
        int_links.append(IntLink(link_id=link_count, node_a=routers[16], node_b=routers[17], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[16], node_b=routers[24], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 17
        int_links.append(IntLink(link_id=link_count, node_a=routers[17], node_b=routers[18], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 18
        int_links.append(IntLink(link_id=link_count, node_a=routers[18], node_b=routers[19], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 19
        int_links.append(IntLink(link_id=link_count, node_a=routers[19], node_b=routers[27], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 21
        int_links.append(IntLink(link_id=link_count, node_a=routers[21], node_b=routers[29], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 24
        int_links.append(IntLink(link_id=link_count, node_a=routers[24], node_b=routers[32], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 29
        int_links.append(IntLink(link_id=link_count, node_a=routers[29], node_b=routers[30], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 30
        int_links.append(IntLink(link_id=link_count, node_a=routers[30], node_b=routers[38], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 32
        int_links.append(IntLink(link_id=link_count, node_a=routers[32], node_b=routers[40], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 38
        int_links.append(IntLink(link_id=link_count, node_a=routers[38], node_b=routers[46], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 40
        int_links.append(IntLink(link_id=link_count, node_a=routers[40], node_b=routers[41], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 41
        int_links.append(IntLink(link_id=link_count, node_a=routers[41], node_b=routers[42], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 42
        int_links.append(IntLink(link_id=link_count, node_a=routers[42], node_b=routers[50], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 46
        int_links.append(IntLink(link_id=link_count, node_a=routers[46], node_b=routers[47], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 50
        int_links.append(IntLink(link_id=link_count, node_a=routers[50], node_b=routers[58], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 58
        int_links.append(IntLink(link_id=link_count, node_a=routers[58], node_b=routers[59], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 59
        int_links.append(IntLink(link_id=link_count, node_a=routers[59], node_b=routers[60], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 60
        int_links.append(IntLink(link_id=link_count, node_a=routers[60], node_b=routers[61], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 61
        int_links.append(IntLink(link_id=link_count, node_a=routers[61], node_b=routers[62], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 62
        int_links.append(IntLink(link_id=link_count, node_a=routers[62], node_b=routers[63], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        network.int_links = int_links
