# Copyright, Georgia Institute of Technology
#
# Authors: Aniruddh Ramrakhyani
#
# 8*8 Mesh with 32 cores on. Config 5: Not Deadlock free

from m5.params import *
from m5.objects import *

from BaseTopology import SimpleTopology

class dark_sillicon_32_mesh_config_5(SimpleTopology):
    description='8*8 Mesh with 32 cores on. Config 5: Not Deadlock free'

    def __init__(self, controllers):
        self.nodes = controllers

    def makeTopology(self, options, network, IntLink, ExtLink, Router):

        assert(options.num_cpus == 32)
        assert(options.num_rows == 8)

        print "Inside dark sillicon 32 mesh config 5"
        
        nodes = self.nodes

        num_routers = 64
        num_rows = 8
        num_columns = 8

        #only 32 cores are on
        num_cpus = 32

        #calculate no of controllers per router
        cntrls_per_router, remainder = divmod(len(nodes), num_cpus)

        # Create the routers in the mesh
        routers = [Router(router_id=i) for i in range(num_routers)]
        network.routers = routers

        # link counter to set unique link ids
        link_count = 0

        #define the core-ids that will be on in the topology
        on_routers_id = []

        # 32 router id assignment statements

        #row 2
        on_routers_id.append(11)
        on_routers_id.append(12)

        #row 3
        on_routers_id.append(17)
        on_routers_id.append(18)
        on_routers_id.append(19)
        on_routers_id.append(20)
        on_routers_id.append(21)
        on_routers_id.append(22)

        #row 4
        on_routers_id.append(25)
        on_routers_id.append(26)
        on_routers_id.append(27)
        on_routers_id.append(28)
        on_routers_id.append(29)
        on_routers_id.append(30)

        #row 5
        on_routers_id.append(33)
        on_routers_id.append(34)
        on_routers_id.append(35)
        on_routers_id.append(36)
        on_routers_id.append(37)
        on_routers_id.append(38)

        #row 6
        on_routers_id.append(41)
        on_routers_id.append(42)
        on_routers_id.append(43)
        on_routers_id.append(44)
        on_routers_id.append(45)
        on_routers_id.append(46)

        #row 7
        on_routers_id.append(49)
        on_routers_id.append(50)
        on_routers_id.append(51)
        on_routers_id.append(52)
        on_routers_id.append(53)
        on_routers_id.append(54)

        #Distribute Network Nodes uniformly across the on-cores
        network_nodes = []
        remainder_nodes = []

        for node_index in xrange(len(nodes)):
            if node_index < (len(nodes) - remainder):
                network_nodes.append(nodes[node_index])
            else:
                remainder_nodes.append(nodes[node_index])

        # Connect each node to the appropriate router
        ext_links = []
        for (i, n) in enumerate(network_nodes):
            index = i % num_cpus
            router_id = on_routers_id[index];
            ext_links.append(ExtLink(link_id=link_count, ext_node=n,int_node=routers[router_id]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        # Connect the remainding nodes to first router that is on.  These should only be
        # DMA nodes.
        for (i, node) in enumerate(remainder_nodes):
            assert(node.type == 'DMA_Controller')
            assert(i < remainder)
            index = on_routers_id(0)
            ext_links.append(ExtLink(link_id=link_count, ext_node=node, int_node=routers[index]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        network.ext_links = ext_links

        #Constants defining port_directions
        west_id = 1
        south_id = 2
        east_id = 3
        north_id = 4

        #Create Internal links: connect the on routers
        int_links = []


        #Create X-dimension links first
        for i in range(17, 54):
            if ( ((i % 8) == 0) or ((i % 8) == 7) or ((i % 8) == 6) ):
                continue
                    
            int_links.append(IntLink(link_id=link_count, node_a=routers[i], node_b=routers[i+1], node_a_port=east_id, node_b_port=west_id, weight=1))
            link_count += 1

        for i in range(17, 47):
            if ( ((i % 8) == 0) or ((i % 8) == 7) ):
                continue

            int_links.append(IntLink(link_id=link_count, node_a=routers[i], node_b=routers[i+8], node_a_port=north_id, node_b_port=south_id, weight=1))
            link_count += 1

        #Router 11
        int_links.append(IntLink(link_id=link_count, node_a=routers[11], node_b=routers[12], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[11], node_b=routers[19], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 12
        int_links.append(IntLink(link_id=link_count, node_a=routers[12], node_b=routers[20], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        network.int_links = int_links
