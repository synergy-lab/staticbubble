# Copyright, Georgia Institute of Technology
#
# Authors: Aniruddh Ramrakhyani
#
# 8*8 FULL Mesh with 16 cores on. Config 2: Deadlock free

from m5.params import *
from m5.objects import *

from BaseTopology import SimpleTopology

class ds_16_FULL_mesh_config_2(SimpleTopology):
    description='8*8 FULL Mesh with 16 cores on. Config 2: Deadlock free'


    def __init__(self, controllers):
        self.nodes = controllers

    def makeTopology(self, options, network, IntLink, ExtLink, Router):

        assert(options.num_cpus == 16)
        assert(options.num_rows == 8)

        print "Inside dark sillicon 16 FULL mesh config 2"
        
        nodes = self.nodes

        num_routers = 64
        num_rows = 8
        num_columns = 8

        #only 16 cores are on
        num_cpus = 16

        #calculate no of controllers per router
        cntrls_per_router, remainder = divmod(len(nodes), num_cpus)

        # Create the routers in the mesh
        routers = [Router(router_id=i) for i in range(num_routers)]
        network.routers = routers

        # link counter to set unique link ids
        link_count = 0

        #define the core-ids that will be on in the topology
        on_routers_id = []

        # 16 router id assignment statements

        #row 1
        on_routers_id.append(7)
        
        #row 2
        on_routers_id.append(14)
        on_routers_id.append(15)

        #row 3
        on_routers_id.append(21)
        on_routers_id.append(22)

        #row 4
        on_routers_id.append(27)
        on_routers_id.append(29)

        #row 5
        on_routers_id.append(35)
        on_routers_id.append(36)
        on_routers_id.append(37)

        #row 6
        on_routers_id.append(42)
        on_routers_id.append(43)

        #row 7
        on_routers_id.append(49)
        on_routers_id.append(50)

        #row 8
        on_routers_id.append(56)
        on_routers_id.append(57)

        #Distribute Network Nodes uniformly across the on-cores
        network_nodes = []
        remainder_nodes = []

        for node_index in xrange(len(nodes)):
            if node_index < (len(nodes) - remainder):
                network_nodes.append(nodes[node_index])
            else:
                remainder_nodes.append(nodes[node_index])

        # Connect each node to the appropriate router
        ext_links = []
        for (i, n) in enumerate(network_nodes):
            index = i % num_cpus
            router_id = on_routers_id[index];
            ext_links.append(ExtLink(link_id=link_count, ext_node=n,int_node=routers[router_id]))
            #print n.type + " connected to router " + str(router_id)
            link_count += 1

        # Connect the remainding nodes to first router that is on.  These should only be
        # DMA nodes.
        for (i, node) in enumerate(remainder_nodes):
            assert(node.type == 'DMA_Controller')
            assert(i < remainder)
            index = on_routers_id(0)
            ext_links.append(ExtLink(link_id=link_count, ext_node=node, int_node=routers[index]))
            #print n.type + " connected to router " + str(router_id)
            link_count += 1

        network.ext_links = ext_links

        # Create the mesh links.  First row (east-west) links then column
        # (north-south) links
        int_links = []
        for row in xrange(num_rows):
            for col in xrange(num_columns):
                if (col + 1 < num_columns):
                    east_id = col + (row * num_columns)
                    west_id = (col + 1) + (row * num_columns)
                    int_links.append(IntLink(link_id=link_count,
                                            node_a=routers[east_id],
                                            node_b=routers[west_id],
                                            node_a_port=3, # east port
                                            node_b_port=1, # west port
                                            weight=1))
                    #print str(east_id) + " connected to " + str(west_id)
                    link_count += 1

        for col in xrange(num_columns):
            for row in xrange(num_rows):
                if (row + 1 < num_rows):
                    north_id = col + (row * num_columns)
                    south_id = col + ((row + 1) * num_columns)
                    int_links.append(IntLink(link_id=link_count,
                                            node_a=routers[north_id],
                                            node_b=routers[south_id],
                                            node_a_port=4, # north port
                                            node_b_port=2, # south port
                                            weight=2))
                    link_count += 1

        network.int_links = int_links
