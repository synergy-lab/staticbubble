# Copyright, Georgia Institute of Technology
#
# Authors: Aniruddh Ramrakhyani
#
# 8*8 Mesh with 32 cores on. Config 3: Not Deadlock free

from m5.params import *
from m5.objects import *

from BaseTopology import SimpleTopology

class dark_sillicon_32_mesh_config_3(SimpleTopology):
    description='8*8 Mesh with 32 cores on. Config 3: Not Deadlock free'

    def __init__(self, controllers):
        self.nodes = controllers

    def makeTopology(self, options, network, IntLink, ExtLink, Router):

        assert(options.num_cpus == 32)
        assert(options.num_rows == 8)

        print "Inside dark sillicon 32 mesh config 3"
        
        nodes = self.nodes

        num_routers = 64
        num_rows = 8
        num_columns = 8

        #only 32 cores are on
        num_cpus = 32

        #calculate no of controllers per router
        cntrls_per_router, remainder = divmod(len(nodes), num_cpus)

        # Create the routers in the mesh
        routers = [Router(router_id=i) for i in range(num_routers)]
        network.routers = routers

        # link counter to set unique link ids
        link_count = 0

        #define the core-ids that will be on in the topology
        on_routers_id = []

        # 32 router id assignment statements
        
        #row 1
        on_routers_id.append(4)
        on_routers_id.append(5)
        on_routers_id.append(6)

        #row 2
        on_routers_id.append(8)
        on_routers_id.append(9)
        on_routers_id.append(10)
        on_routers_id.append(11)
        on_routers_id.append(12)
        on_routers_id.append(13)
        on_routers_id.append(14)

        #row 3
        on_routers_id.append(16)
        on_routers_id.append(17)
        on_routers_id.append(18)
        on_routers_id.append(21)

        #row 4
        on_routers_id.append(24)
        on_routers_id.append(25)
        on_routers_id.append(26)
        on_routers_id.append(29)

        #row 5
        on_routers_id.append(34)
        on_routers_id.append(37)
        on_routers_id.append(38)
        on_routers_id.append(39)

        #row 6
        on_routers_id.append(42)
        on_routers_id.append(45)
        on_routers_id.append(46)
        on_routers_id.append(47)

        #row 7
        on_routers_id.append(50)
        on_routers_id.append(51)
        on_routers_id.append(52)
        on_routers_id.append(53)
        on_routers_id.append(54)
        on_routers_id.append(55)

        #Distribute Network Nodes uniformly across the on-cores
        network_nodes = []
        remainder_nodes = []

        for node_index in xrange(len(nodes)):
            if node_index < (len(nodes) - remainder):
                network_nodes.append(nodes[node_index])
            else:
                remainder_nodes.append(nodes[node_index])

        # Connect each node to the appropriate router
        ext_links = []
        for (i, n) in enumerate(network_nodes):
            index = i % num_cpus
            router_id = on_routers_id[index];
            ext_links.append(ExtLink(link_id=link_count, ext_node=n,int_node=routers[router_id]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        # Connect the remainding nodes to first router that is on.  These should only be
        # DMA nodes.
        for (i, node) in enumerate(remainder_nodes):
            assert(node.type == 'DMA_Controller')
            assert(i < remainder)
            index = on_routers_id(0)
            ext_links.append(ExtLink(link_id=link_count, ext_node=node, int_node=routers[index]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        network.ext_links = ext_links

        #Constants defining port_directions
        west_id = 1
        south_id = 2
        east_id = 3
        north_id = 4

        #Create Internal links: connect the on routers
        int_links = []

        #Router 4
        int_links.append(IntLink(link_id=link_count, node_a=routers[4], node_b=routers[5], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[4], node_b=routers[12], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 5
        int_links.append(IntLink(link_id=link_count, node_a=routers[5], node_b=routers[6], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[5], node_b=routers[13], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 6
        int_links.append(IntLink(link_id=link_count, node_a=routers[6], node_b=routers[14], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 8
        int_links.append(IntLink(link_id=link_count, node_a=routers[8], node_b=routers[9], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[8], node_b=routers[16], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 9
        int_links.append(IntLink(link_id=link_count, node_a=routers[9], node_b=routers[10], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[9], node_b=routers[17], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 10
        int_links.append(IntLink(link_id=link_count, node_a=routers[10], node_b=routers[11], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[10], node_b=routers[18], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 11
        int_links.append(IntLink(link_id=link_count, node_a=routers[11], node_b=routers[12], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 12
        int_links.append(IntLink(link_id=link_count, node_a=routers[12], node_b=routers[13], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 13
        int_links.append(IntLink(link_id=link_count, node_a=routers[13], node_b=routers[14], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[13], node_b=routers[21], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 16
        int_links.append(IntLink(link_id=link_count, node_a=routers[16], node_b=routers[17], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[16], node_b=routers[24], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 17
        int_links.append(IntLink(link_id=link_count, node_a=routers[17], node_b=routers[18], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[17], node_b=routers[25], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 18
        int_links.append(IntLink(link_id=link_count, node_a=routers[18], node_b=routers[26], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 21
        int_links.append(IntLink(link_id=link_count, node_a=routers[21], node_b=routers[29], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 24
        int_links.append(IntLink(link_id=link_count, node_a=routers[24], node_b=routers[25], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 25
        int_links.append(IntLink(link_id=link_count, node_a=routers[25], node_b=routers[26], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 26
        int_links.append(IntLink(link_id=link_count, node_a=routers[26], node_b=routers[34], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 29
        int_links.append(IntLink(link_id=link_count, node_a=routers[29], node_b=routers[37], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 34
        int_links.append(IntLink(link_id=link_count, node_a=routers[34], node_b=routers[42], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 37
        int_links.append(IntLink(link_id=link_count, node_a=routers[37], node_b=routers[38], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[37], node_b=routers[45], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 38
        int_links.append(IntLink(link_id=link_count, node_a=routers[38], node_b=routers[39], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[38], node_b=routers[46], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 39
        int_links.append(IntLink(link_id=link_count, node_a=routers[39], node_b=routers[47], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 42
        int_links.append(IntLink(link_id=link_count, node_a=routers[42], node_b=routers[50], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 45
        int_links.append(IntLink(link_id=link_count, node_a=routers[45], node_b=routers[53], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[45], node_b=routers[46], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 46
        int_links.append(IntLink(link_id=link_count, node_a=routers[46], node_b=routers[47], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[46], node_b=routers[54], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 47
        int_links.append(IntLink(link_id=link_count, node_a=routers[47], node_b=routers[55], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 50
        int_links.append(IntLink(link_id=link_count, node_a=routers[50], node_b=routers[51], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 51
        int_links.append(IntLink(link_id=link_count, node_a=routers[51], node_b=routers[52], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 52
        int_links.append(IntLink(link_id=link_count, node_a=routers[52], node_b=routers[53], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 53
        int_links.append(IntLink(link_id=link_count, node_a=routers[53], node_b=routers[54], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 54
        int_links.append(IntLink(link_id=link_count, node_a=routers[54], node_b=routers[55], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        network.int_links = int_links
