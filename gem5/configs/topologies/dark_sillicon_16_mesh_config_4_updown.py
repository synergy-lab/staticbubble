# Copyright, Georgia Institute of Technology
#
# Authors: Aniruddh Ramrakhyani
#
################## UP-DOWN ######################
#
# 8*8 Mesh with 16 cores on. Config 4: Not Deadlock free

from m5.params import *
from m5.objects import *

from BaseTopology import SimpleTopology

class dark_sillicon_16_mesh_config_4_updown(SimpleTopology):
    description='8*8 Mesh with 16 cores on. Config 4: Not Deadlock free  ###UP-DOWN###'

    def __init__(self, controllers):
        self.nodes = controllers

    def makeTopology(self, options, network, IntLink, ExtLink, Router):

        assert(options.num_cpus == 16)
        assert(options.num_rows == 8)

        print "Inside dark sillicon 16 mesh config 4 ###UP-DOWN###"
        
        nodes = self.nodes

        num_routers = 64
        num_rows = 8
        num_columns = 8

        #only 16 cores are on
        num_cpus = 16

        #calculate no of controllers per router
        cntrls_per_router, remainder = divmod(len(nodes), num_cpus)

        # Create the routers in the mesh
        routers = [Router(router_id=i) for i in range(num_routers)]
        network.routers = routers

        # link counter to set unique link ids
        link_count = 0

        #define the core-ids that will be on in the topology
        on_routers_id = []

        # 16 router id assignment statements

        #row 1
        on_routers_id.append(3)
        on_routers_id.append(4)
        on_routers_id.append(5)
        on_routers_id.append(6)
        on_routers_id.append(7)

        #row 2
        on_routers_id.append(11)
        on_routers_id.append(15)

        #row 3
        on_routers_id.append(19)
        on_routers_id.append(20)
        on_routers_id.append(21)
        on_routers_id.append(23)

        #row 4
        on_routers_id.append(29)
        on_routers_id.append(31)

        #row 5
        on_routers_id.append(37)
        on_routers_id.append(38)
        on_routers_id.append(39)

        #Distribute Network Nodes uniformly across the on-cores
        network_nodes = []
        remainder_nodes = []

        for node_index in xrange(len(nodes)):
            if node_index < (len(nodes) - remainder):
                network_nodes.append(nodes[node_index])
            else:
                remainder_nodes.append(nodes[node_index])

        # Connect each node to the appropriate router
        ext_links = []
        for (i, n) in enumerate(network_nodes):
            index = i % num_cpus
            router_id = on_routers_id[index];
            ext_links.append(ExtLink(link_id=link_count, ext_node=n,int_node=routers[router_id]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        # Connect the remainding nodes to first router that is on.  These should only be
        # DMA nodes.
        for (i, node) in enumerate(remainder_nodes):
            assert(node.type == 'DMA_Controller')
            assert(i < remainder)
            index = on_routers_id(0)
            ext_links.append(ExtLink(link_id=link_count, ext_node=node, int_node=routers[index]))
            print n.type + " connected to router " + str(router_id)
            link_count += 1

        network.ext_links = ext_links

        #Constants defining port_directions
        west_id = 1
        south_id = 2
        east_id = 3
        north_id = 4

        #Create Internal links: connect the on routers
        int_links = []


        #Router 3
        int_links.append(IntLink(link_id=link_count, node_a=routers[3], node_b=routers[11], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        int_links.append(IntLink(link_id=link_count, node_a=routers[3], node_b=routers[4], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 4
        int_links.append(IntLink(link_id=link_count, node_a=routers[4], node_b=routers[5], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 5
        int_links.append(IntLink(link_id=link_count, node_a=routers[5], node_b=routers[6], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 6
        int_links.append(IntLink(link_id=link_count, node_a=routers[6], node_b=routers[7], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 7
        int_links.append(IntLink(link_id=link_count, node_a=routers[7], node_b=routers[15], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 11
        int_links.append(IntLink(link_id=link_count, node_a=routers[11], node_b=routers[19], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 15
        int_links.append(IntLink(link_id=link_count, node_a=routers[15], node_b=routers[23], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 19
        int_links.append(IntLink(link_id=link_count, node_a=routers[19], node_b=routers[20], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 20
#        int_links.append(IntLink(link_id=link_count, node_a=routers[20], node_b=routers[21], node_a_port=east_id, node_b_port=west_id, weight=1))
#        link_count += 1

        #Router 21
        int_links.append(IntLink(link_id=link_count, node_a=routers[21], node_b=routers[29], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 23
        int_links.append(IntLink(link_id=link_count, node_a=routers[23], node_b=routers[31], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 29
        int_links.append(IntLink(link_id=link_count, node_a=routers[29], node_b=routers[37], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 31
        int_links.append(IntLink(link_id=link_count, node_a=routers[31], node_b=routers[39], node_a_port=north_id, node_b_port=south_id, weight=1))
        link_count += 1

        #Router 37
        int_links.append(IntLink(link_id=link_count, node_a=routers[37], node_b=routers[38], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        #Router 38
        int_links.append(IntLink(link_id=link_count, node_a=routers[38], node_b=routers[39], node_a_port=east_id, node_b_port=west_id, weight=1))
        link_count += 1

        network.int_links = int_links
