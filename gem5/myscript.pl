#! /usr/bin/perl

#script for Lab3 ICN

use strict;
use warnings;

my $cmd='echo runnning Lab3';
system($cmd);

$cmd='source ./my_scripts/set_env.cshrc';
system($cmd);

$cmd='./my_scripts/build_Network_test.sh';
system($cmd);

my $text; my $i; my $FHI; my $FHO;

=begin comment

#Part I

for($i=0.02;$i<=0.8;$i=$i+0.04)
{
    $cmd='./build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 \
--topology=Mesh --num-rows=8 --sim-cycles=50000 --injectionrate='.$i.' --synthetic=0 --vcs-per-vnet=8 --routing-algorithm=1';

    system($cmd);

    $cmd='./my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd="grep 'packets_received = ' network_stats.txt >> temp.txt";
    system($cmd);

}

open($FHI,"<","temp.txt");
open($FHO,">","part_a.txt");

while(my $text=<$FHI>)
{
    chomp($text);
    $text=~s|packets_received = ||;
    $text=$text/64;
    $text=$text/50000;
    print $FHO $text;
}

close($FHI);
close($FHO);

=end comment

=cut

#Part II

$cmd='echo > temp.txt';
system($cmd);

for($i=0.02;$i<=0.8;$i=$i+0.04)
{
    $cmd='./build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 \
--topology=Mesh --num-rows=8 --sim-cycles=50000 --injectionrate='.$i.' --synthetic=0 --vcs-per-vnet=8 --routing-algorithm=2';

    system($cmd);

    $cmd='./my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd="grep 'packets_received = ' network_stats.txt >> temp.txt";
    system($cmd);

}

open($FHI,"<","temp.txt");
open($FHO,">","part_b.txt");

while($text=<$FHI>)
{
    chomp($text);
    $text=~s|packets_received = ||;
    $text=$text/64;
    $text=$text/50000;
    print $FHO $text;
}

close($FHI);
close($FHO);

=begin comment

#Part III

$cmd='echo > temp.txt';
system($cmd);

for($i=0.02;$i<=0.8;$i=$i+0.04)
{
    $cmd='./build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 \
--topology=Mesh --num-rows=8 --sim-cycles=50000 --injectionrate='.$i.' --synthetic=0 --vcs-per-vnet=8 --routing-algorithm=3';

    system($cmd);

    $cmd='./my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd="grep 'packets_received = ' network_stats.txt >> temp.txt";
    system($cmd);

}

open($FHI,"<","temp.txt");
open($FHO,">","part_c.txt");

while($text=<$FHI>)
{
    chomp($text);
    $text=~s|packets_received = ||;
    $text=$text/64;
    $text=$text/50000;
    print $FHO $text;
}

close($FHI);
close($FHO);

=end comment
=cut

#Part IV

$cmd='echo > temp.txt';
system($cmd);

for($i=0.02;$i<=0.8;$i=$i+0.04)
{
    $cmd='./build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 \
--topology=Mesh --num-rows=8 --sim-cycles=50000 --injectionrate='.$i.' --synthetic=0 --vcs-per-vnet=8 --routing-algorithm=2 --enable-escape-vc=1';

    system($cmd);

    $cmd='./my_scripts/extract_network_stats.sh';
    system($cmd);

    $cmd="grep 'packets_received = ' network_stats.txt >> temp.txt";
    system($cmd);

}

open($FHI,"<","temp.txt");
open($FHO,">","part_d.txt");

while($text=<$FHI>)
{
    chomp($text);
    $text=~s|packets_received = ||;
    $text=$text/64;
    $text=$text/50000;
    print $FHO $text;
}

close($FHI);
close($FHO);
