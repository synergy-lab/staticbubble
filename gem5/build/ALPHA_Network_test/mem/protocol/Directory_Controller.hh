/** \file Directory_Controller.hh
 *
 * Auto generated C++ code started by /nethome/aramrakhyani3/gem5/src/mem/slicc/symbols/StateMachine.py:277
 * Created by slicc definition of Module "Network_test Directory"
 */

#ifndef __Directory_CONTROLLER_HH__
#define __Directory_CONTROLLER_HH__

#include <iostream>
#include <sstream>
#include <string>

#include "mem/protocol/TransitionResult.hh"
#include "mem/protocol/Types.hh"
#include "mem/ruby/common/Consumer.hh"
#include "mem/ruby/slicc_interface/AbstractController.hh"
#include "params/Directory_Controller.hh"

extern std::stringstream Directory_transitionComment;

class Directory_Controller : public AbstractController
{
  public:
    typedef Directory_ControllerParams Params;
    Directory_Controller(const Params *p);
    static int getNumControllers();
    void init();

    MessageBuffer *getMandatoryQueue() const;
    MessageBuffer *getMemoryQueue() const;
    void initNetQueues();

    void print(std::ostream& out) const;
    void wakeup();
    void resetStats();
    void regStats();
    void collateStats();

    void recordCacheTrace(int cntrl, CacheRecorder* tr);
    Sequencer* getSequencer() const;

    int functionalWriteBuffers(PacketPtr&);

    void countTransition(Directory_State state, Directory_Event event);
    void possibleTransition(Directory_State state, Directory_Event event);
    uint64_t getEventCount(Directory_Event event);
    bool isPossible(Directory_State state, Directory_Event event);
    uint64_t getTransitionCount(Directory_State state, Directory_Event event);

private:
    MessageBuffer* m_requestToDir_ptr;
    MessageBuffer* m_forwardToDir_ptr;
    MessageBuffer* m_responseToDir_ptr;
    TransitionResult doTransition(Directory_Event event,
                                  Addr addr);

    TransitionResult doTransitionWorker(Directory_Event event,
                                        Directory_State state,
                                        Directory_State& next_state,
                                        Addr addr);

    int m_counters[Directory_State_NUM][Directory_Event_NUM];
    int m_event_counters[Directory_Event_NUM];
    bool m_possible[Directory_State_NUM][Directory_Event_NUM];

    static std::vector<Stats::Vector *> eventVec;
    static std::vector<std::vector<Stats::Vector *> > transVec;
    static int m_num_controllers;

    // Internal functions
    Directory_State getState(const Addr& param_addr);
    void setState(const Addr& param_addr, const Directory_State& param_state);
    AccessPermission getAccessPermission(const Addr& param_addr);
    void setAccessPermission(const Addr& param_addr, const Directory_State& param_state);
    void functionalRead(const Addr& param_addr, Packet* param_pkt);
    int functionalWrite(const Addr& param_addr, Packet* param_pkt);

    // Actions
    /** \brief Pop incoming request queue */
    void i_popIncomingRequestQueue(Addr addr);
    /** \brief Pop incoming forward queue */
    void f_popIncomingForwardQueue(Addr addr);
    /** \brief Pop incoming response queue */
    void r_popIncomingResponseQueue(Addr addr);

    // Objects
};
#endif // __Directory_CONTROLLER_H__
