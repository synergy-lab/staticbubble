/** \file L1Cache_State.hh
 *
 * Auto generated C++ code started by /nethome/aramrakhyani3/gem5/src/mem/slicc/symbols/Type.py:545
 */

#include <cassert>
#include <iostream>
#include <string>

#include "base/misc.hh"
#include "mem/protocol/L1Cache_State.hh"

using namespace std;

// Code to convert the current state to an access permission
AccessPermission L1Cache_State_to_permission(const L1Cache_State& obj)
{
    switch(obj) {
      case L1Cache_State_I:
        return AccessPermission_Invalid;
      default:
        panic("Unknown state access permission converstion for L1Cache_State");
    }
}

// Code for output operator
ostream&
operator<<(ostream& out, const L1Cache_State& obj)
{
    out << L1Cache_State_to_string(obj);
    out << flush;
    return out;
}

// Code to convert state to a string
string
L1Cache_State_to_string(const L1Cache_State& obj)
{
    switch(obj) {
      case L1Cache_State_I:
        return "I";
      default:
        panic("Invalid range for type L1Cache_State");
    }
}

// Code to convert from a string to the enumeration
L1Cache_State
string_to_L1Cache_State(const string& str)
{
    if (str == "I") {
        return L1Cache_State_I;
    } else {
        panic("Invalid string conversion for %s, type L1Cache_State", str);
    }
}

// Code to increment an enumeration type
L1Cache_State&
operator++(L1Cache_State& e)
{
    assert(e < L1Cache_State_NUM);
    return e = L1Cache_State(e+1);
}
