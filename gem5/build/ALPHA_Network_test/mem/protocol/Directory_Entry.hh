/** \file Directory_Entry.hh
 *
 *
 * Auto generated C++ code started by /nethome/aramrakhyani3/gem5/src/mem/slicc/symbols/Type.py:195
 */

#ifndef __Directory_Entry_HH__
#define __Directory_Entry_HH__

#include <iostream>

#include "mem/ruby/slicc_interface/RubySlicc_Util.hh"
#include "mem/protocol/Directory_State.hh"
#include "mem/protocol/DataBlock.hh"
#include "mem/protocol/AbstractEntry.hh"
class Directory_Entry :  public AbstractEntry
{
  public:
    Directory_Entry
()
		{
        m_DirectoryState = Directory_State_I; // default value of Directory_State
        // m_DataBlk has no default
    }
    Directory_Entry(const Directory_Entry&other)
        : AbstractEntry(other)
    {
        m_DirectoryState = other.m_DirectoryState;
        m_DataBlk = other.m_DataBlk;
    }
    Directory_Entry(const Directory_State& local_DirectoryState, const DataBlock& local_DataBlk)
        : AbstractEntry()
    {
        m_DirectoryState = local_DirectoryState;
        m_DataBlk = local_DataBlk;
    }
    Directory_Entry*
    clone() const
    {
         return new Directory_Entry(*this);
    }
    // Const accessors methods for each field
    /** \brief Const accessor method for DirectoryState field.
     *  \return DirectoryState field
     */
    const Directory_State&
    getDirectoryState() const
    {
        return m_DirectoryState;
    }
    /** \brief Const accessor method for DataBlk field.
     *  \return DataBlk field
     */
    const DataBlock&
    getDataBlk() const
    {
        return m_DataBlk;
    }
    // Non const Accessors methods for each field
    /** \brief Non-const accessor method for DirectoryState field.
     *  \return DirectoryState field
     */
    Directory_State&
    getDirectoryState()
    {
        return m_DirectoryState;
    }
    /** \brief Non-const accessor method for DataBlk field.
     *  \return DataBlk field
     */
    DataBlock&
    getDataBlk()
    {
        return m_DataBlk;
    }
    // Mutator methods for each field
    /** \brief Mutator method for DirectoryState field */
    void
    setDirectoryState(const Directory_State& local_DirectoryState)
    {
        m_DirectoryState = local_DirectoryState;
    }
    /** \brief Mutator method for DataBlk field */
    void
    setDataBlk(const DataBlock& local_DataBlk)
    {
        m_DataBlk = local_DataBlk;
    }
    void print(std::ostream& out) const;
  //private:
    /** Directory state */
    Directory_State m_DirectoryState;
    /** data for the block */
    DataBlock m_DataBlk;
};
inline std::ostream&
operator<<(std::ostream& out, const Directory_Entry& obj)
{
    obj.print(out);
    out << std::flush;
    return out;
}

#endif // __Directory_Entry_HH__
