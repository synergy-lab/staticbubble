// Auto generated C++ code started by /nethome/aramrakhyani3/gem5/src/mem/slicc/symbols/StateMachine.py:999
// L1Cache: Network_test L1 Cache

#include <sys/types.h>
#include <unistd.h>

#include <cassert>
#include <typeinfo>

#include "base/misc.hh"

#include "debug/RubySlicc.hh"
#include "debug/RubyGenerated.hh"
#include "mem/protocol/L1Cache_Controller.hh"
#include "mem/protocol/L1Cache_Event.hh"
#include "mem/protocol/L1Cache_State.hh"

#include "mem/protocol/Types.hh"
#include "mem/ruby/system/RubySystem.hh"

#include "mem/ruby/slicc_interface/RubySlicc_includes.hh"

using namespace std;

void
L1Cache_Controller::wakeup()
{
    int counter = 0;
    while (true) {
        unsigned char rejected[1];
        memset(rejected, 0, sizeof(unsigned char)*1);
        // Some cases will put us into an infinite loop without this limit
        assert(counter <= m_transitions_per_cycle);
        if (counter == m_transitions_per_cycle) {
            // Count how often we are fully utilized
            m_fully_busy_cycles++;

            // Wakeup in another cycle and try again
            scheduleEvent(Cycles(1));
            break;
        }
            // L1CacheInPort mandatoryQueue_in
            m_cur_in_port = 0;
            try {
                            if ((((*m_mandatoryQueue_ptr)).isReady((clockEdge())))) {
                                {
                                    // Declare message
                                    const RubyRequest* in_msg_ptr M5_VAR_USED;
                                    in_msg_ptr = dynamic_cast<const RubyRequest *>(((*m_mandatoryQueue_ptr)).peek());
                                    if (in_msg_ptr == NULL) {
                                        // If the cast fails, this is the wrong inport (wrong message type).
                                        // Throw an exception, and the caller will decide to either try a
                                        // different inport or punt.
                                        throw RejectException();
                                    }
                                {

                                    TransitionResult result = doTransition((mandatory_request_type_to_event(((*in_msg_ptr)).m_Type)), (getCacheEntry(((*in_msg_ptr)).m_LineAddress)), ((*in_msg_ptr)).m_LineAddress);

                                    if (result == TransitionResult_Valid) {
                                        counter++;
                                        continue; // Check the first port again
                                    }

                                    if (result == TransitionResult_ResourceStall ||
                                        result == TransitionResult_ProtocolStall) {
                                        scheduleEvent(Cycles(1));

                                        // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                    }
                                }
                                ;
                                }
                            }
                        } catch (const RejectException & e) {
                            rejected[0]++;
                        }
        // If we got this far, we have nothing left todo or something went
        // wrong
        break;
    }
}
