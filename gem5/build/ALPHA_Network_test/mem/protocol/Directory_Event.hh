/** \file Directory_Event.hh
 *
 * Auto generated C++ code started by /nethome/aramrakhyani3/gem5/src/mem/slicc/symbols/Type.py:448
 */

#ifndef __Directory_Event_HH__
#define __Directory_Event_HH__

#include <iostream>
#include <string>


// Class definition
/** \enum Directory_Event
 *  \brief Directory events
 */
enum Directory_Event {
    Directory_Event_FIRST,
    Directory_Event_Receive_Request = Directory_Event_FIRST, /**< Receive Message */
    Directory_Event_Receive_Forward, /**< Receive Message */
    Directory_Event_Receive_Response, /**< Receive Message */
    Directory_Event_NUM
};

// Code to convert from a string to the enumeration
Directory_Event string_to_Directory_Event(const std::string& str);

// Code to convert state to a string
std::string Directory_Event_to_string(const Directory_Event& obj);

// Code to increment an enumeration type
Directory_Event &operator++(Directory_Event &e);
std::ostream& operator<<(std::ostream& out, const Directory_Event& obj);

#endif // __Directory_Event_HH__
