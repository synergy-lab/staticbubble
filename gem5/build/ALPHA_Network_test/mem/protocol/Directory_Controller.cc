/** \file Directory_Controller.cc
 *
 * Auto generated C++ code started by /nethome/aramrakhyani3/gem5/src/mem/slicc/symbols/StateMachine.py:459
 * Created by slicc definition of Module "Network_test Directory"
 */

#include <sys/types.h>
#include <unistd.h>

#include <cassert>
#include <sstream>
#include <string>
#include <typeinfo>

#include "base/compiler.hh"
#include "base/cprintf.hh"

#include "debug/RubySlicc.hh"
#include "debug/RubyGenerated.hh"
#include "mem/protocol/Directory_Controller.hh"
#include "mem/protocol/Directory_Event.hh"
#include "mem/protocol/Directory_State.hh"
#include "mem/protocol/Types.hh"
#include "mem/ruby/system/RubySystem.hh"

#include "mem/ruby/slicc_interface/RubySlicc_includes.hh"

using namespace std;
Directory_Controller *
Directory_ControllerParams::create()
{
    return new Directory_Controller(this);
}

int Directory_Controller::m_num_controllers = 0;
std::vector<Stats::Vector *>  Directory_Controller::eventVec;
std::vector<std::vector<Stats::Vector *> >  Directory_Controller::transVec;

// for adding information to the protocol debug trace
stringstream Directory_transitionComment;

#ifndef NDEBUG
#define APPEND_TRANSITION_COMMENT(str) (Directory_transitionComment << str)
#else
#define APPEND_TRANSITION_COMMENT(str) do {} while (0)
#endif

/** \brief constructor */
Directory_Controller::Directory_Controller(const Params *p)
    : AbstractController(p)
{
    m_machineID.type = MachineType_Directory;
    m_machineID.num = m_version;
    m_num_controllers++;

    m_in_ports = 3;
    m_requestToDir_ptr = p->requestToDir;
    m_forwardToDir_ptr = p->forwardToDir;
    m_responseToDir_ptr = p->responseToDir;

    for (int state = 0; state < Directory_State_NUM; state++) {
        for (int event = 0; event < Directory_Event_NUM; event++) {
            m_possible[state][event] = false;
            m_counters[state][event] = 0;
        }
    }
    for (int event = 0; event < Directory_Event_NUM; event++) {
        m_event_counters[event] = 0;
    }
}

void
Directory_Controller::initNetQueues()
{
    MachineType machine_type = string_to_MachineType("Directory");
    int base M5_VAR_USED = MachineType_base_number(machine_type);

    assert(m_requestToDir_ptr != NULL);
    m_net_ptr->setFromNetQueue(m_version + base, m_requestToDir_ptr->getOrdered(), 0,
                                     "request", m_requestToDir_ptr);
    assert(m_forwardToDir_ptr != NULL);
    m_net_ptr->setFromNetQueue(m_version + base, m_forwardToDir_ptr->getOrdered(), 1,
                                     "forward", m_forwardToDir_ptr);
    assert(m_responseToDir_ptr != NULL);
    m_net_ptr->setFromNetQueue(m_version + base, m_responseToDir_ptr->getOrdered(), 2,
                                     "response", m_responseToDir_ptr);
}

void
Directory_Controller::init()
{
    // initialize objects


    (*m_requestToDir_ptr).setConsumer(this);
    (*m_forwardToDir_ptr).setConsumer(this);
    (*m_responseToDir_ptr).setConsumer(this);

    possibleTransition(Directory_State_I, Directory_Event_Receive_Request);
    possibleTransition(Directory_State_I, Directory_Event_Receive_Forward);
    possibleTransition(Directory_State_I, Directory_Event_Receive_Response);
    AbstractController::init();
    resetStats();
}

void
Directory_Controller::regStats()
{
    AbstractController::regStats();

    if (m_version == 0) {
        for (Directory_Event event = Directory_Event_FIRST;
             event < Directory_Event_NUM; ++event) {
            Stats::Vector *t = new Stats::Vector();
            t->init(m_num_controllers);
            t->name(params()->ruby_system->name() + ".Directory_Controller." +
                Directory_Event_to_string(event));
            t->flags(Stats::pdf | Stats::total | Stats::oneline |
                     Stats::nozero);

            eventVec.push_back(t);
        }

        for (Directory_State state = Directory_State_FIRST;
             state < Directory_State_NUM; ++state) {

            transVec.push_back(std::vector<Stats::Vector *>());

            for (Directory_Event event = Directory_Event_FIRST;
                 event < Directory_Event_NUM; ++event) {

                Stats::Vector *t = new Stats::Vector();
                t->init(m_num_controllers);
                t->name(params()->ruby_system->name() + ".Directory_Controller." +
                        Directory_State_to_string(state) +
                        "." + Directory_Event_to_string(event));

                t->flags(Stats::pdf | Stats::total | Stats::oneline |
                         Stats::nozero);
                transVec[state].push_back(t);
            }
        }
    }
}

void
Directory_Controller::collateStats()
{
    for (Directory_Event event = Directory_Event_FIRST;
         event < Directory_Event_NUM; ++event) {
        for (unsigned int i = 0; i < m_num_controllers; ++i) {
            RubySystem *rs = params()->ruby_system;
            std::map<uint32_t, AbstractController *>::iterator it =
                     rs->m_abstract_controls[MachineType_Directory].find(i);
            assert(it != rs->m_abstract_controls[MachineType_Directory].end());
            (*eventVec[event])[i] =
                ((Directory_Controller *)(*it).second)->getEventCount(event);
        }
    }

    for (Directory_State state = Directory_State_FIRST;
         state < Directory_State_NUM; ++state) {

        for (Directory_Event event = Directory_Event_FIRST;
             event < Directory_Event_NUM; ++event) {

            for (unsigned int i = 0; i < m_num_controllers; ++i) {
                RubySystem *rs = params()->ruby_system;
                std::map<uint32_t, AbstractController *>::iterator it =
                         rs->m_abstract_controls[MachineType_Directory].find(i);
                assert(it != rs->m_abstract_controls[MachineType_Directory].end());
                (*transVec[state][event])[i] =
                    ((Directory_Controller *)(*it).second)->getTransitionCount(state, event);
            }
        }
    }
}

void
Directory_Controller::countTransition(Directory_State state, Directory_Event event)
{
    assert(m_possible[state][event]);
    m_counters[state][event]++;
    m_event_counters[event]++;
}
void
Directory_Controller::possibleTransition(Directory_State state,
                             Directory_Event event)
{
    m_possible[state][event] = true;
}

uint64_t
Directory_Controller::getEventCount(Directory_Event event)
{
    return m_event_counters[event];
}

bool
Directory_Controller::isPossible(Directory_State state, Directory_Event event)
{
    return m_possible[state][event];
}

uint64_t
Directory_Controller::getTransitionCount(Directory_State state,
                             Directory_Event event)
{
    return m_counters[state][event];
}

int
Directory_Controller::getNumControllers()
{
    return m_num_controllers;
}

MessageBuffer*
Directory_Controller::getMandatoryQueue() const
{
    return NULL;
}

MessageBuffer*
Directory_Controller::getMemoryQueue() const
{
    return NULL;
}

Sequencer*
Directory_Controller::getSequencer() const
{
    return NULL;
}

void
Directory_Controller::print(ostream& out) const
{
    out << "[Directory_Controller " << m_version << "]";
}

void Directory_Controller::resetStats()
{
    for (int state = 0; state < Directory_State_NUM; state++) {
        for (int event = 0; event < Directory_Event_NUM; event++) {
            m_counters[state][event] = 0;
        }
    }

    for (int event = 0; event < Directory_Event_NUM; event++) {
        m_event_counters[event] = 0;
    }

    AbstractController::resetStats();
}

void
Directory_Controller::recordCacheTrace(int cntrl, CacheRecorder* tr)
{
}

// Actions
/** \brief Pop incoming request queue */
void
Directory_Controller::i_popIncomingRequestQueue(Addr addr)
{
    DPRINTF(RubyGenerated, "executing i_popIncomingRequestQueue\n");
    (((*m_requestToDir_ptr)).dequeue((clockEdge())));

}

/** \brief Pop incoming forward queue */
void
Directory_Controller::f_popIncomingForwardQueue(Addr addr)
{
    DPRINTF(RubyGenerated, "executing f_popIncomingForwardQueue\n");
    (((*m_forwardToDir_ptr)).dequeue((clockEdge())));

}

/** \brief Pop incoming response queue */
void
Directory_Controller::r_popIncomingResponseQueue(Addr addr)
{
    DPRINTF(RubyGenerated, "executing r_popIncomingResponseQueue\n");
    (((*m_responseToDir_ptr)).dequeue((clockEdge())));

}

Directory_State
Directory_Controller::getState(const Addr& param_addr)
{
return Directory_State_I;

}
void
Directory_Controller::setState(const Addr& param_addr, const Directory_State& param_state)
{

}
AccessPermission
Directory_Controller::getAccessPermission(const Addr& param_addr)
{
return AccessPermission_NotPresent;

}
void
Directory_Controller::setAccessPermission(const Addr& param_addr, const Directory_State& param_state)
{

}
void
Directory_Controller::functionalRead(const Addr& param_addr, Packet* param_pkt)
{
panic("Runtime Error at Network_test-dir.sm:82: %s.\n", ("Network test does not support functional read."));
;

}
int
Directory_Controller::functionalWrite(const Addr& param_addr, Packet* param_pkt)
{
panic("Runtime Error at Network_test-dir.sm:86: %s.\n", ("Network test does not support functional write."));
;

}
int
Directory_Controller::functionalWriteBuffers(PacketPtr& pkt)
{
    int num_functional_writes = 0;
num_functional_writes += m_requestToDir_ptr->functionalWrite(pkt);
num_functional_writes += m_forwardToDir_ptr->functionalWrite(pkt);
num_functional_writes += m_responseToDir_ptr->functionalWrite(pkt);
    return num_functional_writes;
}
