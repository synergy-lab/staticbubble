/** \file RequestMsg.cc
 *
 * Auto generated C++ code started by /nethome/aramrakhyani3/gem5/src/mem/slicc/symbols/Type.py:402
 */

#include <iostream>
#include <memory>

#include "mem/protocol/RequestMsg.hh"
#include "mem/ruby/system/RubySystem.hh"

using namespace std;
/** \brief Print the state of this object */
void
RequestMsg::print(ostream& out) const
{
    out << "[RequestMsg: ";
    out << "addr = " << printAddress(m_addr) << " ";
    out << "Type = " << m_Type << " ";
    out << "Requestor = " << m_Requestor << " ";
    out << "Destination = " << m_Destination << " ";
    out << "DataBlk = " << m_DataBlk << " ";
    out << "MessageSize = " << m_MessageSize << " ";
    out << "]";
}
bool
RequestMsg::functionalWrite(Packet* param_pkt)
{
panic("Runtime Error at Network_test-msg.sm:49: %s.\n", ("Network test does not support functional accesses!"));
;

}
bool
RequestMsg::functionalRead(Packet* param_pkt)
{
panic("Runtime Error at Network_test-msg.sm:45: %s.\n", ("Network test does not support functional accesses!"));
;

}
