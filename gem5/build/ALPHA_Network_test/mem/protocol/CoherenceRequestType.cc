/** \file CoherenceRequestType.hh
 *
 * Auto generated C++ code started by /nethome/aramrakhyani3/gem5/src/mem/slicc/symbols/Type.py:545
 */

#include <cassert>
#include <iostream>
#include <string>

#include "base/misc.hh"
#include "mem/protocol/CoherenceRequestType.hh"

using namespace std;

// Code for output operator
ostream&
operator<<(ostream& out, const CoherenceRequestType& obj)
{
    out << CoherenceRequestType_to_string(obj);
    out << flush;
    return out;
}

// Code to convert state to a string
string
CoherenceRequestType_to_string(const CoherenceRequestType& obj)
{
    switch(obj) {
      case CoherenceRequestType_MSG:
        return "MSG";
      default:
        panic("Invalid range for type CoherenceRequestType");
    }
}

// Code to convert from a string to the enumeration
CoherenceRequestType
string_to_CoherenceRequestType(const string& str)
{
    if (str == "MSG") {
        return CoherenceRequestType_MSG;
    } else {
        panic("Invalid string conversion for %s, type CoherenceRequestType", str);
    }
}

// Code to increment an enumeration type
CoherenceRequestType&
operator++(CoherenceRequestType& e)
{
    assert(e < CoherenceRequestType_NUM);
    return e = CoherenceRequestType(e+1);
}
