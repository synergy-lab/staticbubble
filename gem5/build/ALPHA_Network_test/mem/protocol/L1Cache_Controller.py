from m5.params import *
from m5.SimObject import SimObject
from Controller import RubyController

class L1Cache_Controller(RubyController):
    type = 'L1Cache_Controller'
    cxx_header = 'mem/protocol/L1Cache_Controller.hh'
    sequencer = Param.RubySequencer("")
    issue_latency = Param.Cycles((2), "")
    requestFromCache = Param.MessageBuffer("")
    forwardFromCache = Param.MessageBuffer("")
    responseFromCache = Param.MessageBuffer("")
    mandatoryQueue = Param.MessageBuffer("")
