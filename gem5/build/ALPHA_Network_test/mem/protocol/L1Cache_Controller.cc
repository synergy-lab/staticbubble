/** \file L1Cache_Controller.cc
 *
 * Auto generated C++ code started by /nethome/aramrakhyani3/gem5/src/mem/slicc/symbols/StateMachine.py:459
 * Created by slicc definition of Module "Network_test L1 Cache"
 */

#include <sys/types.h>
#include <unistd.h>

#include <cassert>
#include <sstream>
#include <string>
#include <typeinfo>

#include "base/compiler.hh"
#include "base/cprintf.hh"

#include "debug/RubySlicc.hh"
#include "debug/RubyGenerated.hh"
#include "mem/protocol/L1Cache_Controller.hh"
#include "mem/protocol/L1Cache_Event.hh"
#include "mem/protocol/L1Cache_State.hh"
#include "mem/protocol/Types.hh"
#include "mem/ruby/system/RubySystem.hh"

#include "mem/ruby/slicc_interface/RubySlicc_includes.hh"

using namespace std;
#include "mem/protocol/DataBlock.hh"
L1Cache_Controller *
L1Cache_ControllerParams::create()
{
    return new L1Cache_Controller(this);
}

int L1Cache_Controller::m_num_controllers = 0;
std::vector<Stats::Vector *>  L1Cache_Controller::eventVec;
std::vector<std::vector<Stats::Vector *> >  L1Cache_Controller::transVec;

// for adding information to the protocol debug trace
stringstream L1Cache_transitionComment;

#ifndef NDEBUG
#define APPEND_TRANSITION_COMMENT(str) (L1Cache_transitionComment << str)
#else
#define APPEND_TRANSITION_COMMENT(str) do {} while (0)
#endif

/** \brief constructor */
L1Cache_Controller::L1Cache_Controller(const Params *p)
    : AbstractController(p)
{
    m_machineID.type = MachineType_L1Cache;
    m_machineID.num = m_version;
    m_num_controllers++;

    m_in_ports = 1;
    m_sequencer_ptr = p->sequencer;
    m_sequencer_ptr->setController(this);
    m_issue_latency = p->issue_latency;
    m_requestFromCache_ptr = p->requestFromCache;
    m_forwardFromCache_ptr = p->forwardFromCache;
    m_responseFromCache_ptr = p->responseFromCache;
    m_mandatoryQueue_ptr = p->mandatoryQueue;

    for (int state = 0; state < L1Cache_State_NUM; state++) {
        for (int event = 0; event < L1Cache_Event_NUM; event++) {
            m_possible[state][event] = false;
            m_counters[state][event] = 0;
        }
    }
    for (int event = 0; event < L1Cache_Event_NUM; event++) {
        m_event_counters[event] = 0;
    }
}

void
L1Cache_Controller::initNetQueues()
{
    MachineType machine_type = string_to_MachineType("L1Cache");
    int base M5_VAR_USED = MachineType_base_number(machine_type);

    assert(m_requestFromCache_ptr != NULL);
    m_net_ptr->setToNetQueue(m_version + base, m_requestFromCache_ptr->getOrdered(), 0,
                                     "request", m_requestFromCache_ptr);
    assert(m_forwardFromCache_ptr != NULL);
    m_net_ptr->setToNetQueue(m_version + base, m_forwardFromCache_ptr->getOrdered(), 1,
                                     "forward", m_forwardFromCache_ptr);
    assert(m_responseFromCache_ptr != NULL);
    m_net_ptr->setToNetQueue(m_version + base, m_responseFromCache_ptr->getOrdered(), 2,
                                     "response", m_responseFromCache_ptr);
}

void
L1Cache_Controller::init()
{
    // initialize objects
    m_dummyData_ptr  = new DataBlock();
    assert(m_dummyData_ptr != NULL);


    (*m_mandatoryQueue_ptr).setConsumer(this);

    possibleTransition(L1Cache_State_I, L1Cache_Event_Response);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Request);
    possibleTransition(L1Cache_State_I, L1Cache_Event_Forward);
    AbstractController::init();
    resetStats();
}

void
L1Cache_Controller::regStats()
{
    AbstractController::regStats();

    if (m_version == 0) {
        for (L1Cache_Event event = L1Cache_Event_FIRST;
             event < L1Cache_Event_NUM; ++event) {
            Stats::Vector *t = new Stats::Vector();
            t->init(m_num_controllers);
            t->name(params()->ruby_system->name() + ".L1Cache_Controller." +
                L1Cache_Event_to_string(event));
            t->flags(Stats::pdf | Stats::total | Stats::oneline |
                     Stats::nozero);

            eventVec.push_back(t);
        }

        for (L1Cache_State state = L1Cache_State_FIRST;
             state < L1Cache_State_NUM; ++state) {

            transVec.push_back(std::vector<Stats::Vector *>());

            for (L1Cache_Event event = L1Cache_Event_FIRST;
                 event < L1Cache_Event_NUM; ++event) {

                Stats::Vector *t = new Stats::Vector();
                t->init(m_num_controllers);
                t->name(params()->ruby_system->name() + ".L1Cache_Controller." +
                        L1Cache_State_to_string(state) +
                        "." + L1Cache_Event_to_string(event));

                t->flags(Stats::pdf | Stats::total | Stats::oneline |
                         Stats::nozero);
                transVec[state].push_back(t);
            }
        }
    }
}

void
L1Cache_Controller::collateStats()
{
    for (L1Cache_Event event = L1Cache_Event_FIRST;
         event < L1Cache_Event_NUM; ++event) {
        for (unsigned int i = 0; i < m_num_controllers; ++i) {
            RubySystem *rs = params()->ruby_system;
            std::map<uint32_t, AbstractController *>::iterator it =
                     rs->m_abstract_controls[MachineType_L1Cache].find(i);
            assert(it != rs->m_abstract_controls[MachineType_L1Cache].end());
            (*eventVec[event])[i] =
                ((L1Cache_Controller *)(*it).second)->getEventCount(event);
        }
    }

    for (L1Cache_State state = L1Cache_State_FIRST;
         state < L1Cache_State_NUM; ++state) {

        for (L1Cache_Event event = L1Cache_Event_FIRST;
             event < L1Cache_Event_NUM; ++event) {

            for (unsigned int i = 0; i < m_num_controllers; ++i) {
                RubySystem *rs = params()->ruby_system;
                std::map<uint32_t, AbstractController *>::iterator it =
                         rs->m_abstract_controls[MachineType_L1Cache].find(i);
                assert(it != rs->m_abstract_controls[MachineType_L1Cache].end());
                (*transVec[state][event])[i] =
                    ((L1Cache_Controller *)(*it).second)->getTransitionCount(state, event);
            }
        }
    }
}

void
L1Cache_Controller::countTransition(L1Cache_State state, L1Cache_Event event)
{
    assert(m_possible[state][event]);
    m_counters[state][event]++;
    m_event_counters[event]++;
}
void
L1Cache_Controller::possibleTransition(L1Cache_State state,
                             L1Cache_Event event)
{
    m_possible[state][event] = true;
}

uint64_t
L1Cache_Controller::getEventCount(L1Cache_Event event)
{
    return m_event_counters[event];
}

bool
L1Cache_Controller::isPossible(L1Cache_State state, L1Cache_Event event)
{
    return m_possible[state][event];
}

uint64_t
L1Cache_Controller::getTransitionCount(L1Cache_State state,
                             L1Cache_Event event)
{
    return m_counters[state][event];
}

int
L1Cache_Controller::getNumControllers()
{
    return m_num_controllers;
}

MessageBuffer*
L1Cache_Controller::getMandatoryQueue() const
{
    return m_mandatoryQueue_ptr;
}

MessageBuffer*
L1Cache_Controller::getMemoryQueue() const
{
    return NULL;
}

Sequencer*
L1Cache_Controller::getSequencer() const
{
    return m_sequencer_ptr;
}

void
L1Cache_Controller::print(ostream& out) const
{
    out << "[L1Cache_Controller " << m_version << "]";
}

void L1Cache_Controller::resetStats()
{
    for (int state = 0; state < L1Cache_State_NUM; state++) {
        for (int event = 0; event < L1Cache_Event_NUM; event++) {
            m_counters[state][event] = 0;
        }
    }

    for (int event = 0; event < L1Cache_Event_NUM; event++) {
        m_event_counters[event] = 0;
    }

    AbstractController::resetStats();
}

// Set and Reset for cache_entry variable
void
L1Cache_Controller::set_cache_entry(L1Cache_Entry*& m_cache_entry_ptr, AbstractCacheEntry* m_new_cache_entry)
{
  m_cache_entry_ptr = (L1Cache_Entry*)m_new_cache_entry;
}

void
L1Cache_Controller::unset_cache_entry(L1Cache_Entry*& m_cache_entry_ptr)
{
  m_cache_entry_ptr = 0;
}

void
L1Cache_Controller::recordCacheTrace(int cntrl, CacheRecorder* tr)
{
}

// Actions
/** \brief Issue a request */
void
L1Cache_Controller::a_issueRequest(L1Cache_Entry*& m_cache_entry_ptr, Addr addr)
{
    DPRINTF(RubyGenerated, "executing a_issueRequest\n");
    {
    std::shared_ptr<RequestMsg> out_msg = std::make_shared<RequestMsg>(clockEdge());
    (*out_msg).m_addr = addr;
    (*out_msg).m_Type = CoherenceRequestType_MSG;
    (*out_msg).m_Requestor = m_machineID;
    (((*out_msg).m_Destination).add((map_Address_to_Directory(addr))));
    (*out_msg).m_MessageSize = MessageSizeType_Control;
    ((*m_requestFromCache_ptr)).enqueue(out_msg, clockEdge(), cyclesToTicks(Cycles(m_issue_latency)));
}

}

/** \brief Issue a forward */
void
L1Cache_Controller::b_issueForward(L1Cache_Entry*& m_cache_entry_ptr, Addr addr)
{
    DPRINTF(RubyGenerated, "executing b_issueForward\n");
    {
    std::shared_ptr<RequestMsg> out_msg = std::make_shared<RequestMsg>(clockEdge());
    (*out_msg).m_addr = addr;
    (*out_msg).m_Type = CoherenceRequestType_MSG;
    (*out_msg).m_Requestor = m_machineID;
    (((*out_msg).m_Destination).add((map_Address_to_Directory(addr))));
    (*out_msg).m_MessageSize = MessageSizeType_Control;
    ((*m_forwardFromCache_ptr)).enqueue(out_msg, clockEdge(), cyclesToTicks(Cycles(m_issue_latency)));
}

}

/** \brief Issue a response */
void
L1Cache_Controller::c_issueResponse(L1Cache_Entry*& m_cache_entry_ptr, Addr addr)
{
    DPRINTF(RubyGenerated, "executing c_issueResponse\n");
    {
    std::shared_ptr<RequestMsg> out_msg = std::make_shared<RequestMsg>(clockEdge());
    (*out_msg).m_addr = addr;
    (*out_msg).m_Type = CoherenceRequestType_MSG;
    (*out_msg).m_Requestor = m_machineID;
    (((*out_msg).m_Destination).add((map_Address_to_Directory(addr))));
    (*out_msg).m_MessageSize = MessageSizeType_Data;
    ((*m_responseFromCache_ptr)).enqueue(out_msg, clockEdge(), cyclesToTicks(Cycles(m_issue_latency)));
}

}

/** \brief Pop the mandatory request queue */
void
L1Cache_Controller::m_popMandatoryQueue(L1Cache_Entry*& m_cache_entry_ptr, Addr addr)
{
    DPRINTF(RubyGenerated, "executing m_popMandatoryQueue\n");
    (((*m_mandatoryQueue_ptr)).dequeue((clockEdge())));

}

/** \brief Notify sequencer the load completed. */
void
L1Cache_Controller::r_load_hit(L1Cache_Entry*& m_cache_entry_ptr, Addr addr)
{
    DPRINTF(RubyGenerated, "executing r_load_hit\n");
    (((*m_sequencer_ptr)).readCallback(addr, (*m_dummyData_ptr)));

}

/** \brief Notify sequencer that store completed. */
void
L1Cache_Controller::s_store_hit(L1Cache_Entry*& m_cache_entry_ptr, Addr addr)
{
    DPRINTF(RubyGenerated, "executing s_store_hit\n");
    (((*m_sequencer_ptr)).writeCallback(addr, (*m_dummyData_ptr)));

}

L1Cache_Event
L1Cache_Controller::mandatory_request_type_to_event(const RubyRequestType& param_type)
{
    if ((param_type == RubyRequestType_LD)) {
        return L1Cache_Event_Request;
    } else {
            if ((param_type == RubyRequestType_IFETCH)) {
                return L1Cache_Event_Forward;
            } else {
                    if ((param_type == RubyRequestType_ST)) {
                        return L1Cache_Event_Response;
                    } else {
                        panic("Runtime Error at Network_test-cache.sm:93: %s.\n", ("Invalid RubyRequestType"));
                        ;
                    }
                }
            }

}
L1Cache_State
L1Cache_Controller::getState(L1Cache_Entry* param_cache_entry, const Addr& param_addr)
{
return L1Cache_State_I;

}
void
L1Cache_Controller::setState(L1Cache_Entry* param_cache_entry, const Addr& param_addr, const L1Cache_State& param_state)
{

}
AccessPermission
L1Cache_Controller::getAccessPermission(const Addr& param_addr)
{
return AccessPermission_NotPresent;

}
void
L1Cache_Controller::setAccessPermission(L1Cache_Entry* param_cache_entry, const Addr& param_addr, const L1Cache_State& param_state)
{

}
L1Cache_Entry*
L1Cache_Controller::getCacheEntry(const Addr& param_address)
{
return NULL;

}
void
L1Cache_Controller::functionalRead(const Addr& param_addr, Packet* param_pkt)
{
panic("Runtime Error at Network_test-cache.sm:118: %s.\n", ("Network test does not support functional read."));
;

}
int
L1Cache_Controller::functionalWrite(const Addr& param_addr, Packet* param_pkt)
{
panic("Runtime Error at Network_test-cache.sm:122: %s.\n", ("Network test does not support functional write."));
;

}
int
L1Cache_Controller::functionalWriteBuffers(PacketPtr& pkt)
{
    int num_functional_writes = 0;
num_functional_writes += m_requestFromCache_ptr->functionalWrite(pkt);
num_functional_writes += m_forwardFromCache_ptr->functionalWrite(pkt);
num_functional_writes += m_responseFromCache_ptr->functionalWrite(pkt);
num_functional_writes += m_mandatoryQueue_ptr->functionalWrite(pkt);
    return num_functional_writes;
}
