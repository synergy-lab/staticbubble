%module(package="m5.internal") AlphaInterrupts_vector
%{
#include "params/AlphaInterrupts.hh"
%}

%include "std_container.i"

%import "python/m5/internal/param_AlphaInterrupts.i"

%include "std_vector.i"

%template(vector_AlphaInterrupts) std::vector< AlphaISA::Interrupts * >;
