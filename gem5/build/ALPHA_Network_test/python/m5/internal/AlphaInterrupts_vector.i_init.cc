#include "sim/init.hh"

extern "C" {
    void init_AlphaInterrupts_vector();
}

EmbeddedSwig embed_swig_AlphaInterrupts_vector(init_AlphaInterrupts_vector);
