#ifndef __PARAMS__Directory_Controller__
#define __PARAMS__Directory_Controller__

class Directory_Controller;

#include <cstddef>
#include "params/MessageBuffer.hh"
#include <cstddef>
#include "params/MessageBuffer.hh"
#include <cstddef>
#include "params/MessageBuffer.hh"

#include "params/RubyController.hh"

struct Directory_ControllerParams
    : public RubyControllerParams
{
    Directory_Controller * create();
    MessageBuffer * forwardToDir;
    MessageBuffer * requestToDir;
    MessageBuffer * responseToDir;
};

#endif // __PARAMS__Directory_Controller__
