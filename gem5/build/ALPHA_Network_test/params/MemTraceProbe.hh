#ifndef __PARAMS__MemTraceProbe__
#define __PARAMS__MemTraceProbe__

class MemTraceProbe;

#include <cstddef>
#include <cstddef>
#include <string>

#include "params/BaseMemProbe.hh"

struct MemTraceProbeParams
    : public BaseMemProbeParams
{
    MemTraceProbe * create();
    bool trace_compress;
    std::string trace_file;
};

#endif // __PARAMS__MemTraceProbe__
