#ifndef __PARAMS__BasicIntLink__
#define __PARAMS__BasicIntLink__

class BasicIntLink;

#include <cstddef>
#include "params/BasicRouter.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "params/BasicRouter.hh"
#include <cstddef>
#include "base/types.hh"

#include "params/BasicLink.hh"

struct BasicIntLinkParams
    : public BasicLinkParams
{
    BasicIntLink * create();
    BasicRouter * node_a;
    int node_a_port;
    BasicRouter * node_b;
    int node_b_port;
};

#endif // __PARAMS__BasicIntLink__
