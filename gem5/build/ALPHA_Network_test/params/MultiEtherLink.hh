#ifndef __PARAMS__MultiEtherLink__
#define __PARAMS__MultiEtherLink__

class MultiEtherLink;

#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "params/EtherDump.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include <string>
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"

#include "params/EtherObject.hh"

struct MultiEtherLinkParams
    : public EtherObjectParams
{
    MultiEtherLink * create();
    Tick delay;
    Tick delay_var;
    EtherDump * dump;
    uint32_t multi_rank;
    std::string server_name;
    uint32_t server_port;
    float speed;
    Tick sync_repeat;
    Tick sync_start;
    unsigned int port_int0_connection_count;
};

#endif // __PARAMS__MultiEtherLink__
