// DO NOT EDIT
// This file was automatically generated from an ISA description:
//   alpha/isa/main.isa

namespace AlphaISAInst {
    const int MaxInstSrcRegs = 3;
    const int MaxInstDestRegs = 2;
    const int MaxMiscDestRegs = 1;
}
