#! /usr/bin/perl

use strict;
use warnings;

# script to determine the percentage of deadlock-prone topologies 
# look at injection rate=0.9 for each topology for each num_fault case
# if packets_received + total_flits_dropped < packets_injected, it is deadlock prone

my $cmd = 'echo > results.txt';
system($cmd);

for(my $num_faults =1; $num_faults <49; $num_faults++)
{
    my $path = '/usr/scratch/aniruddh/hpca_results/camera_ready_results/percent_deadlock_heat_map/routers/fault_'.$num_faults;

    my $count =0;

    # there are 100 random topologies for each fault number
    for(my $top_num =1; $top_num <101; $top_num++)
    {
	my $file = $path.'/t'.$top_num.'.txt';

	open(my $fh, "<", $file) or die $file;

	while(my $line = <$fh>)
	{
	    chomp($line);

	    if($line eq 'injection rate =0.9')
	    {
		<$fh>;
		<$fh>;
		
		$line = <$fh>;
		chomp($line);

		$line =~ s/^\s+//;
		$line =~ s/\s+$//;

		my $packets_injected;
		
		#if(length($line) > 19)
		#{
		    $packets_injected = substr($line,19);
		#}
		#else
		#{
		#    print($file);
		#    exit;
		#}
		

		$line = <$fh>;
		chomp($line);

		$line =~ s/^\s+//;
		$line =~ s/\s+$//;
		
		my $packets_rcvd = substr($line,19);

		for(my $i=0; $i<23; $i++)
		{
		    <$fh>;
		}
		
		$line = <$fh>;
		chomp($line);

		$line =~ s/^\s+//;
		$line =~ s/\s+$//;

		#my $packets_dropped;

		#if(length($line) > 22)
		#{
		    my $packets_dropped = substr($line,22);
		#}
		#else
		#{
		#    print($file);
		#    exit;
		#}

		if($packets_rcvd + $packets_dropped > $packets_injected)
		{
		    print($file);
		    exit;
		}

		if($packets_rcvd + $packets_dropped < $packets_injected)
		{
		    $count++;
		}
	    }
	}
	close($fh);
    }

    $cmd ='echo '.$count.' >> results.txt';
    system($cmd);
}
