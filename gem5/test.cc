#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char* argv[])
{
  float num_faults = 10.0f;

  float prob = num_faults/112.0f;

  double exp_val = 0.0f;
  
  for(int i=0; i<=10; i++)
    {
      exp_val += prob * ((double)i);
    }

  cout<<exp_val<<"\n";

  return 0;
}
