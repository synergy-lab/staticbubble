#! /usr/bin/perl

use strict;
use warnings;

# script to determine performance loss between minnimal routing and BFS

# Avg. Performance loss: For each fault-number calclulate the average of avg. flit latency for 10 different topologies.

# Max. Performance loss: Out of all topologies for a given fault number, calculate the max flit latency for BFS and min for minimal.

my $path = '/usr/scratch/aniruddh/per_loss';

#my $cmd = 'mkdir '.$path;
#system($cmd);

# There will be two output files corresponding to injection rate of 0.01 and 0.02

my $cmd = 'echo > '.$path.'/inj001_sb.txt';
system($cmd);
$cmd = 'echo > '.$path.'/inj001_evc.txt';
system($cmd);
$cmd = 'echo > '.$path.'/inj001_bfs.txt';
system($cmd);

$cmd = 'echo > '.$path.'/inj002_sb.txt';
system($cmd);
$cmd = 'echo > '.$path.'/inj002_evc.txt';
system($cmd);
$cmd = 'echo > '.$path.'/inj002_bfs.txt';
system($cmd);

$cmd = 'echo > '.$path.'/num_tops_1.txt';
system($cmd);

$cmd = 'echo > '.$path.'/num_tops_2.txt';
system($cmd);

for(my $num_faults = 1; $num_faults<100; $num_faults++)
{
    # inj=0.01
    my $sb_flit_latency_1 = 0.0;
    my $bfs_flit_latency_1 = 0.0;
    my $evc_flit_latency_1 = 0.0;

    # inj=0.02
    my $sb_flit_latency_2 = 0.0;
    my $bfs_flit_latency_2 = 0.0;
    my $evc_flit_latency_2 = 0.0;


    my $ex_diff_1 = -1000;
    my $ex_diff_2 = -1000;

    for(my $i=1; $i<99; $i++)
    {
#escape vc
	my $file = '/usr/scratch/aniruddh/hpca_results/latency_low_load/links/escapevc/fault_'.$num_faults.'/t'.$i.'.txt';
	open(my $evc_fh, $file) or print($file);
	my @evc_temp = read_file($evc_fh);
	close($evc_fh);

#static bubble
	my $file_1 = '/usr/scratch/aniruddh/hpca_results/latency_low_load/links/sb/fault_'.$num_faults.'/t'.$i.'.txt';
	open(my $sb_fh, $file_1) or print($file_1);
	my @sb_temp = read_file($sb_fh);
	close($sb_fh);

#bfs
	my $file_2 = '/usr/scratch/aniruddh/hpca_results/latency_low_load/links/bfs/fault_'.$num_faults.'/t'.$i.'.txt';
	open(my $bfs_fh, $file_2) or print($file_2);
	my @bfs_temp = read_file($bfs_fh);
	close($bfs_fh);

	if(!($evc_temp[0]==-1 || $sb_temp[0]==-1 || $bfs_temp[0]==-1))
	{	    
	    if( ($evc_temp[0] - $sb_temp[0]) > $ex_diff_1)
	    {
		$ex_diff_1 = $evc_temp[0] - $sb_temp[0];
		$sb_flit_latency_1 = $sb_temp[0];
		$evc_flit_latency_1 = $evc_temp[0];
		$bfs_flit_latency_1 = $bfs_temp[0];
	    }
	}

	if(!($evc_temp[1]==-1 || $sb_temp[1]==-1 || $bfs_temp[1]==-1))
	{
	    if( ($evc_temp[1] - $sb_temp[1]) > $ex_diff_2)
	    {
		$ex_diff_2 = $evc_temp[1] - $sb_temp[1];
		$sb_flit_latency_2 = $sb_temp[1];
		$evc_flit_latency_2 = $evc_temp[1];
		$bfs_flit_latency_2 = $bfs_temp[1];
	    }
	}
    }

    $cmd ='echo '.$sb_flit_latency_1.' >> '.$path.'/inj001_sb.txt';
    system($cmd);
    $cmd ='echo '.$bfs_flit_latency_1.' >> '.$path.'/inj001_bfs.txt';
    system($cmd);
    $cmd ='echo '.$evc_flit_latency_1.' >> '.$path.'/inj001_evc.txt';
    system($cmd);

    $cmd ='echo '.$sb_flit_latency_2.' >> '.$path.'/inj002_sb.txt';
    system($cmd);
    $cmd ='echo '.$bfs_flit_latency_2.' >> '.$path.'/inj002_bfs.txt';
    system($cmd);
    $cmd ='echo '.$evc_flit_latency_2.' >> '.$path.'/inj002_evc.txt';
    system($cmd);

#    $cmd = 'echo '.$num_topologies_1.' >> '.$path.'/num_tops_1.txt';
#    system($cmd);

#    $cmd = 'echo '.$num_topologies_2.' >> '.$path.'/num_tops_2.txt';
#    system($cmd);
}

sub read_file 
{
    my $fh = $_[0];

    my @avg_latency;

    while(my $line = <$fh>)
    {
	if(index($line, 'injection rate =0.01') != -1)
	{	
	    for(my $j=1; $j<=4; $j++)
	    {
		<$fh>;
	    }
	    
	    $line = <$fh>;
	    chomp($line);
	    
	    $line =~ s/^\s+//;
	    $line =~ s/\s+$//;
	    
	    if(length($line) >= 25)
	    {
		#if($avg_flit_latency_2 < substr($line,25))
		#{
		#    $avg_latency[1] = substr($line,25);
		#}
		$avg_latency[0] = substr($line,25);
	    }
	    else
	    {
		$avg_latency[0] = -1;
	    }
	    
	}
	
	elsif(index($line, 'injection rate =0.02') != -1)
	{
	    for(my $j=1; $j<=4; $j++)
	    {
		<$fh>;
	    }
	    
	    $line = <$fh>;
	    chomp($line);
	    
	    $line =~ s/^\s+//;
	    $line =~ s/\s+$//;	    
	    
	    if(length($line) >= 25)
	    {
		#if($avg_flit_latency_2 < substr($line,25))
		#{
		#    $avg_latency[1] = substr($line,25);
		#}
		$avg_latency[1] = substr($line,25);
	    }
	    else
	    {
		$avg_latency[1] = -1;
	    }
	    
	    last;
	}
    }

    return @avg_latency;
    
}
