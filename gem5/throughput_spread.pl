#! /usr/bin/perl

use strict;
use warnings;

# script to determine performance loss between minnimal routing and BFS

# Avg. Performance loss: For each fault-number calclulate the average of avg. flit latency for 10 different topologies.

# Max. Performance loss: Out of all topologies for a given fault number, calculate the max flit latency for BFS and min for minimal.

my $path = '/usr/scratch/aniruddh/per_loss';

#my $cmd = 'mkdir '.$path;
#system($cmd);

# There will be two output files corresponding to injection rate of 0.01 and 0.02

my $cmd = 'echo > '.$path.'/throughput_avg.txt';
system($cmd);

$cmd = 'echo > '.$path.'/perfect.txt';
system($cmd);

$cmd = 'echo > '.$path.'/too_high.txt';
system($cmd);

$cmd = 'echo > '.$path.'/too_low.txt';
system($cmd);

#my @faults = (2, 7, 15, 20, 30);

#for(my $iter = 0; $iter<5; $iter++)
for(my $num_faults =6; $num_faults<20; $num_faults++)
{
    #my $num_faults = $faults[$iter];

    my $throughput_avg = 0.0;

    my $max_throughput = 0.0;

    my $perfect = 0;
    my $too_high =0;
    my $too_low =0;

    for(my $i=1; $i<21; $i++)
    {
	my $file = '/usr/scratch/aniruddh/hpca_results/throughput/routers/escapevc/fault_'.$num_faults.'/t'.$i.'.txt';
	open(my $fh, $file) or print($file);

	my $first =1;
	my $throughput_present = 0;
	my $prev_inj_rate = 0.0;

	while(my $line = <$fh>)
	{
	    if(index($line, 'injection rate =') != -1)
	    {
		chomp($line);
		
		$line =~ s/^\s+//;
		$line =~ s/\s+$//;

		my $inj_rate = substr($line, 16);

		for(my $j=1; $j<=4; $j++)
		{
		    <$fh>;
		}
		
		$line = <$fh>;
		chomp($line);

		$line =~ s/^\s+//;
		$line =~ s/\s+$//;

		if(length($line) >= 25)
		{
		    my $avg_flit_latency = substr($line,25);

		    if($first == 1)
		    {
			if($avg_flit_latency > 200.0)
			{
			    $too_high++;
			    $throughput_present = 1;
			    last;
			}
			$first = 0;
		    }

		    if( ($avg_flit_latency > 70.0) && ($avg_flit_latency < 200.0) )
		    {
			$throughput_avg = $throughput_avg + $inj_rate;

			if($inj_rate > $max_throughput)
			{
			    $max_throughput = $inj_rate;
			}

			$perfect++;
			$throughput_present = 1;
			last;
		    }

		    if($avg_flit_latency > 200.0)
		    {
			my $diff = $inj_rate - $prev_inj_rate;
			$throughput_avg = $throughput_avg + $prev_inj_rate + ($diff/2);

			my $new_inj_rate = $prev_inj_rate + ($diff/2);

			if($new_inj_rate > $max_throughput)
			{
			    $max_throughput = $new_inj_rate;
			}


			$throughput_present = 1;
			$perfect++;
			last;
		    }
		    
		    $prev_inj_rate = $inj_rate;
		}
	    }
	}
	
	if($throughput_present == 0)
	{
	    $too_low++;
	}
	
	close($fh);

    }
    

    if($perfect > 0)
    {
	$throughput_avg = $throughput_avg / $perfect;
    }


    $cmd ='echo '.$throughput_avg.' >> '.$path.'/throughput_avg.txt';
    system($cmd);

    $cmd ='echo '.$perfect.' >> '.$path.'/perfect.txt';
    system($cmd);

    $cmd = 'echo '.$too_high.' >> '.$path.'/too_high.txt';
    system($cmd);

    $cmd = 'echo '.$too_low.' >> '.$path.'/too_low.txt';
    system($cmd);

    print($max_throughput);
    print("\n");
}
