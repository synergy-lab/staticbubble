#! /usr/bin/perl

# For link failures: SB

# script to lauch screens in parallel. For each fault number launch a new screen and call another script

#my @faults = (3, 10, 15, 20, 30, 40); #for links

my @faults = (2, 5, 8, 12, 15, 20); #for routers

my $scr_name = 'sb';

for(my $iter =0; $iter < 6; $iter++)
{
    my $fault_num = $faults[$iter];

    my $cmd = 'screen -m -d -S '.$scr_name.$fault_num;
    system($cmd);

    $cmd = 'screen -S '.$scr_name.$fault_num.' -p 0 -X stuff "tcsh
"';
    system($cmd); 

    $cmd = 'screen -S '.$scr_name.$fault_num.' -p 0 -X stuff "source ./my_scripts/set_env.cshrc
"';
    system($cmd);

    # make a new directory for each fault num

    my $path = '/usr/scratch/aniruddh/hpca_results/rodinia/routers/bfs/kmeans/fault_'.$fault_num;

    $cmd = 'mkdir -p '.$path;
    system($cmd);

    $cmd = 'mkdir -p '.$path.'/sim';
    system($cmd);

    $cmd = 'mkdir -p '.$path.'/sim/temp';
    system($cmd);


    my $start_inj_rate;
    my $end_inj_rate;
    my $step;

 if($fault_num == 2)
    {
	$start_inj_rate = 0.2;
	$end_inj_rate = 0.56;
	$step = 0.02;
    }
    elsif($fault_num == 5)
    {
	$start_inj_rate = 0.2;
	$end_inj_rate = 0.56;
	$step = 0.02;
    }
    elsif($fault_num == 8)
    {
	$start_inj_rate = 0.2;
	$end_inj_rate = 0.56;
	$step = 0.02;
    }
    elsif($fault_num == 12)
    {
	$start_inj_rate = 0.2;
	$end_inj_rate = 0.56;
	$step = 0.02;
    }
    elsif($fault_num == 15)
    {
	$start_inj_rate = 0.2;
	$end_inj_rate = 0.56;
	$step = 0.02;
    }
    elsif($fault_num == 20)
    {
	$start_inj_rate = 0.2;
	$end_inj_rate = 0.58;
	$step = 0.02;
    }


    for(my $ir=$start_inj_rate; $ir<$end_inj_rate; $ir = $ir + $step)
    {
	my $num = $ir * 1000;

	if($num>=100)
	{
	    $cmd = 'mkdir -p '.$path.'/sim/inj0'.$num;
	}
	elsif($num>=10)
	{
	    $cmd = 'mkdir -p '.$path.'/sim/inj00'.$num;
	}
	else
	{
	    $cmd = 'mkdir -p '.$path.'/sim/inj000'.$num;
	}
	
	system($cmd);
    }
    #$cmd = 'mkdir -p '.$path.'/sim/inj001';
    #system($cmd);

    #$cmd = 'mkdir -p '.$path.'/sim/inj002';
    #system($cmd);

    $cmd = 'screen -S '.$scr_name.$fault_num.' -p 0 -X stuff "perl /nethome/aramrakhyani3/gem5/sb_fault_script_h3.pl '.$fault_num.'
"';
    system($cmd);
}
