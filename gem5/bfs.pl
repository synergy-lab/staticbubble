#! /usr/bin/perl

use strict;
use warnings;

# script to run bfs topologies.

# Sweep num-faults from 1 to 100. Create 10 diff topologies for each fault number. Run the simulation
# for injection rate of 0.01 and 0.02 for each topology

for(my $num_faults = 1; $num_faults<101; $num_faults++)
{
    # For each fault create a new directory
    my $path = '/usr/scratch/aniruddh/bfs/fault_'.$num_faults;
    my $cmd; #= 'mkdir '.$path;
    #system($cmd);

    # generate 100 different topologies for each fault by changing the seed
    for(my $seed=11; $seed<101; $seed++)
    {
	$cmd = 'echo > '.$path.'/t'.$seed.'.txt';
	system($cmd);

	#create topology using the bfs.cc file

	$cmd = './bfs_code '.$num_faults.' '.$seed;
	system($cmd);

       # for each topology vary the injection rate from 0.01 to 0.02
	for(my $ir=0.01; $ir<=0.02; $ir = $ir + 0.01)
	{
	    $cmd = 'echo ------------------Sim--------------------------- >> '.$path.'/t'.$seed.'.txt';
	    system($cmd);

	    $cmd = 'echo >> '.$path.'/t'.$seed.'.txt';
	    system($cmd);

	    $cmd = 'echo injection rate ='.$ir.' >> '.$path.'/t'.$seed.'.txt';
	    system($cmd);

	    $cmd = './build/ALPHA_Network_test/gem5.debug configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 --topology=bfs --num-rows=8 --vcs-per-vnet=8 --sim-cycles=10000 --injectionrate='.$ir.' --synthetic=0 --routing-algorithm=0 --enable-sb-fault-model=1 --num-packets-max=63';

	    system($cmd);

	    $cmd = './my_scripts/extract_network_stats.sh';
	    system($cmd);

	    $cmd = 'echo >> '.$path.'/t'.$seed.'.txt';
	    system($cmd);

	    $cmd = 'cat network_stats.txt >> '.$path.'/t'.$seed.'.txt';
	    system($cmd);

	    $cmd = 'echo >> '.$path.'/t'.$seed.'.txt';
	    system($cmd);

	    $cmd = 'echo --------------------end------------------ >> '.$path.'/t'.$seed.'.txt';
	    system($cmd);

	    $cmd = 'echo >> '.$path.'/t'.$seed.'.txt';
	    system($cmd);

	}

    }

}
