#! /usr/bin/perl

use strict;
use warnings;

# run simulation for topologies with 1 to 100 faults
# For each fault number generate 100 different topologies 
# For each configuration sweep the injection rate from 0.02 to 0.4 in steps of 0.02

# Edit: changed
# run simulation for topologies with 1 to 48 faulty routers
# For each fault number generate 100 different topologies 
# For each configuration sweep the injection rate from 0.1 to 0.9 in steps of 0.1


# For LINKS

my $fault_num = $ARGV[0];

my $path = '/usr/scratch/aniruddh/hpca_results/rodinia/routers/escapevc/srad/fault_'.$fault_num;

my $faulty_links_file = $path.'/faulty_links.txt';

my $sim_path = $path.'/sim/temp';

my $network_stats_path = $sim_path.'/network_stats.txt';

#my $sim_file_inj001 = $path.'/sim/inj001';
#my $sim_file_inj002 = $path.'/sim/inj002';

my $cmd; #= 'mkdir '.$path;
#system($cmd);

my $start_inj_rate;
my $end_inj_rate;
my $step;

 if($fault_num == 2)
    {
	$start_inj_rate = 0.2;
	$end_inj_rate = 0.44;
	$step = 0.02;
    }
    elsif($fault_num == 5)
    {
	$start_inj_rate = 0.2;
	$end_inj_rate = 0.44;
	$step = 0.02;
    }
    elsif($fault_num == 8)
    {
	$start_inj_rate = 0.2;
	$end_inj_rate = 0.44;
	$step = 0.02;
    }
    elsif($fault_num == 12)
    {
	$start_inj_rate = 0.1;
	$end_inj_rate = 0.4;
	$step = 0.02;
    }
    elsif($fault_num == 15)
    {
	$start_inj_rate = 0.1;
	$end_inj_rate = 0.4;
	$step = 0.02;
    }
    elsif($fault_num == 20)
    {
	$start_inj_rate = 0.14;
	$end_inj_rate = 0.4;
	$step = 0.02;
    }



# generate 100 different topologies for each fault by changing the seed
for(my $seed=1; $seed<21; $seed++)
{
    $cmd = 'echo > '.$path.'/t'.$seed.'.txt';
    system($cmd);
    
    #$cmd = './bfs_routers '.$fault_num.' '.$seed.' '.$faulty_links_file;
    #system($cmd);
    
    # for each topology vary the injection rate from 0.02 to 0.5 in steps of 0.02
    for(my $ir=$start_inj_rate; $ir<$end_inj_rate; $ir = $ir + $step)
    {
	$cmd = 'echo ------------------Sim--------------------------- >> '.$path.'/t'.$seed.'.txt';
	system($cmd);
	
	$cmd = 'echo >> '.$path.'/t'.$seed.'.txt';
	system($cmd);
	
	$cmd = 'echo injection rate ='.$ir.' >> '.$path.'/t'.$seed.'.txt';
	system($cmd);
	
	# cmd for bfs
	#$cmd = './build/ALPHA_Network_test/gem5.debug -d '.$sim_path.' configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 --topology=bfs --num-rows=8 --vcs-per-vnet=4 --sim-cycles=10000 --injectionrate='.$ir.' --synthetic=8 --routing-algorithm=0 --enable-sb-fault-model=1 --faulty-links-file='.$faulty_links_file;

	# cmd for sb
	$cmd = './build/ALPHA_Network_test/gem5.debug -d '.$sim_path.' configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 --topology=faulty_routers --num-rows=8 --vcs-per-vnet=3 --sim-cycles=10000 --injectionrate='.$ir.' --synthetic=11 --routing-algorithm=0 --enable-sb-fault-model=1 --enable-static-bubble=1 --dd-thresh=126 --seed='.$seed.' --num-faults='.$fault_num;


	# cmd for escapevc : 3 vc per vnet
	# $cmd = './build/ALPHA_Network_test/gem5.debug -d '.$sim_path.' configs/example/ruby_network_test.py --network=garnet2.0 --num-cpus=64 --num-dirs=64 --topology=faulty_routers --num-rows=8 --vcs-per-vnet=3 --sim-cycles=10000 --injectionrate='.$ir.' --synthetic=0 --routing-algorithm=0 --enable-sb-fault-model=1 --enable-static-bubble=1 --dd-thresh=126 --seed='.$seed.' --num-faults='.$num_faults;
	
	system($cmd);
	
	$cmd = './my_scripts/extract_network_stats.sh '.$sim_path.' '.$network_stats_path;
	system($cmd);
	$cmd = 'echo >> '.$path.'/t'.$seed.'.txt';
	system($cmd);
	
	$cmd = 'cat '.$network_stats_path.' >> '.$path.'/t'.$seed.'.txt';
	system($cmd);

	my $name= $ir*1000;

	my $sim_file_inj;

	if($name>=100)
	{
	    $sim_file_inj = $path.'/sim/inj0'.$name;
	}
	elsif($name>=10)
	{
	    $sim_file_inj = $path.'/sim/inj00'.$name;
	}
	else
	{
	    $sim_file_inj = $path.'/sim/inj000'.$name;
	}


	$cmd = 'cp '.$sim_path.'/stats.txt '.$sim_file_inj.'/t'.$seed.'.txt';
	system($cmd);
	
	#if($ir == 0.01)
	#{
	#    $cmd = 'cp '.$sim_path.'/stats.txt '.$sim_file_inj001.'/t'.$seed.'.txt';
	#    system($cmd);
	#}
	
	#if($ir == 0.02)
	#{
	#    $cmd = 'cp '.$sim_path.'/stats.txt '.$sim_file_inj002.'/t'.$seed.'.txt';
	#    system($cmd);
	#}
	
	$cmd = 'echo >> '.$path.'/t'.$seed.'.txt';
	system($cmd);
	
	$cmd = 'echo --------------------end------------------ >> '.$path.'/t'.$seed.'.txt';
	system($cmd);
	
	$cmd = 'echo >> '.$path.'/t'.$seed.'.txt';
	system($cmd);
    }
}
